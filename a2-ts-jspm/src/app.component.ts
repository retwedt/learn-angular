// app.component

import { Component } from '@angular/core';

@Component({
    selector: 'my-app',
    template: `
        <h1>{{title}}</h1>
        <p>This is app.component!</p>
    `
})


export class AppComponent {
    title = 'My Angular2 App';
}