# Install process for Angular2-Typescript-JSPM pipeline


## Things to have installed globally beforehand:

- Node.js v6.6.2
- npm v3.9.5
- typescript v1.8.10
- typings v1.3.0
- tslint v3.11.0
- jspm@beta v0.17.0-beta.22
- stylus v0.54.5
- ? autoprefixer-stylus v0.9.3
- ? s-grid v1.2.0
- ? karma v0.13.22
- ? ava v0.15.2
- ? protractor v3.3.0


## Setup

1. `npm init`
2. `npm install jspm@beta --save-dev`
3. `jspm init`
    - config file saved as `config/jspm.config.js`
4. `jspm install` the following packages
    - `npm:@angular/core`
    - `npm:@angular/common`
    - `npm:@angular/compiler`
    - `npm:@angular/forms`
    - `npm:@angular/http`
    - `npm:@angular/platform-browser`
    - `npm:@angular/platform-browser-dynamic`
    - `npm:@angular/router`
    - `npm:@angular/upgrade`
    - `es6-shim`
    - `github:frankwallis/plugin-typescript`
    - `reflect-metadata`
    - `zone.js`
    - `rxjs`
    - `npm:ramda`
5. `npm install --save-dev` the following packages
    - `typings`
    - `lite-server`
    - `tslint`
    - `chokidar`
    - `chokidar-cli`
    - `concurrently`
    - `karma`
    - `ava`
    - `protractor`
    - `tslint-eslint-rules`
    - `mkdirp`
    - `stylus`
    - `autoprefixer-stylus`
    - `s-grid`
    - `clang-format`
6. run `ava --init` to add ava to the project
7. Add an "ava" section to your `package.json` file, with Ava config options
    - Options can be found [here](https://github.com/avajs/ava#configuration)
8. (*** On Ubuntu ***) Run the following script:
```
echo fs.inotify.max_user_watches=524288 | sudo tee -a /etc/sysctl.conf && sudo sysctl -p
```
    - This solves an 'insufficient memory error' that was popping up when the server was started.  [issue](https://github.com/johnpapa/lite-server/issues/9) [source](https://stackoverflow.com/questions/34335340/angular2-quickstart-npm-start-is-not-working-correctly/35636790#35636790)


## Include the following scripts in your `package.json` file

- "test": "tsc test/*.ts && ava",
- "postinstall": "jspm i && typings i",
- "lite-server": "lite-server -c config/bs-config.js",
- "bundle": "jspm bundle app build/main.js -id && npm run lint",
- "bundle:prod": "jspm bundle app build/main.min.js --minify",
- "styles:global": "stylus -u s-grid -u autoprefixer-stylus styles/core.styl --out styles/styles.css -m",
- "styles:app": "./node_modules/mkdirp/bin/cmd.js build/styles && stylus -u s-grid -u autoprefixer-stylus src/ --out build/styles -m",
- "styles": "npm run styles:global && npm run styles:app",
- "lint": "tslint -c config/tslint.json 'src/**/*.ts?(x)' --force",
- "watch-ts": "chokidar \"src/**/*.ts\" -c \"npm run bundle\"",
- "watch-styles:global": "chokidar \"styles/*.styl\" -c \"npm run styles:global\"",
- "watch-styles:app": "chokidar \"src/**/*.styl\" -c \"npm run styles:app\"",
- "watch": "concurrently \"npm run watch-ts\" \"npm run watch-styles:global\" \"npm run watch-styles:app\"",
- "dev": "npm run bundle && npm run styles && concurrently \"npm run watch\" \"npm run lite-server\"",
- "dev:ts": "concurrently \"npm run watch-ts\" \"npm run lite-server\"",
- "dev:style": "concurrently \"npm run watch-styles:app\" \"npm run lite-server\""


## Additional notes:

- Use JSPM to manage the app dependencies, and npm for any development tools (server, linter, file-watch, etc.)
- Use `app` for the name of the project when setting up `jspm.config.js`
- Add `typescriptOptions` section to jspm.config.js file
```
  typescriptOptions: {
    "tsconfig": "config/tsconfig.json",
    "module": "system"
  }
```
- Put all scripts at the end of the body in `index.html`
    - This solves an error 'EXCEPTION: Error during instantiation of AnimationDriver!' caused by trying to manipulate the DOM before it is actually loaded.  [source](https://stackoverflow.com/questions/37944263/jspm-angular2-rc2-build-animate-error)
- Use `jspm r <package-name>` to remove a package, and then `jspm clean` to clean up your config files