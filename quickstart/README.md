# Quickstart

This is the Quickstart project from the Angular2 Documentation page.

To test this project:

1. Run `npm i` to install dependencies
2. Run `npm start` to build the project, start a local server, and begin monitoring files for changes

[Tutorial Link](https://angular.io/docs/ts/latest/quickstart.html)
