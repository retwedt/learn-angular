import {Component} from '@angular/core';

@Component({
    selector: 'loop-back',
    template: `
        <div class='loop-back-wrap'>
            Loop-back
            <input #box (keyup)='0'>
            <p>{{box.value}}</p>
        </div>
    `,
    styles: [`
        .loop-back-wrap {
            margin: 6px;
        }
    `]
})

export class LoopBackComponent {}