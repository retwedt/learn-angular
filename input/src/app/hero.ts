export class Hero {
    constructor(public id: number,
                public name: string) {}

    // also could be written this way:
    // id: number;
    // name: string;
}