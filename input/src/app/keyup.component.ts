import {Component} from '@angular/core';




//--Key-Up----------------------------------------------------------------------
//------------------------------------------------------------------------------

@Component({
    selector: 'key-up',
    template: `
        <div class='key-up-wrap'>
            Key Up #1
            <input (keyup)='OnKey($event)'>
            <p>{{values}}</p>
        </div>
    `,
    styles: [`
        .key-up-wrap {
            margin: 6px;
        }
    `]
})

export class KeyUpComponent {
    values = '';

    OnKey(event: any) {
        this.values += event.target.value + ' | ';
    }

    // strongly typed
    // OnKey(event: KeyboardEvent) {
    //     this.values += (<HTMLInputElement>event.target).value + ' | ';
    // }
}




//--Key-Up-2--------------------------------------------------------------------
//------------------------------------------------------------------------------

@Component({
    selector: 'key-up-2',
    template: `
        <div class='key-up-2-wrap'>
            Key Up #2
            <input #box (keyup)='OnKey(box.value)'>
            <p>{{box.value}}</p>
        </div>
    `,
    styles: [`
        .key-up-2-wrap {
            margin: 6px;
        }
    `]
})

export class KeyUp2Component {
    values = '';

    OnKey(value: string) {
        this.values += value + ' | ';
    }
}




//--Key-Up-3--------------------------------------------------------------------
//------------------------------------------------------------------------------

@Component({
    selector: 'key-up-3',
    template: `
        <div class='key-up-3-wrap'>
            Key Up #3
            <input #box
                (keyup.enter)='OnKey(box.value)'
                (blur)='OnKey(box.value)'>
            <p>{{values}}</p>
        </div>
    `,
    styles: [`
        .key-up-3-wrap {
            margin: 6px;
        }
    `]
})

export class KeyUp3Component {
    values = '';

    OnKey(value: string) {
        this.values = value;
    }
}