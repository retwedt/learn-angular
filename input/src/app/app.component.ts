
import { Component } from '@angular/core';


import {Hero} from './hero';

import {ClickMeComponent} from './click-me.component';
import {KeyUpComponent, KeyUp2Component, KeyUp3Component} from './keyup.component';
import {LoopBackComponent} from './loop-back.component';
import {LittleTourComponent} from './little-tour.component';

import {FriendService} from './friend.service';
import {FriendComponent} from './friend.component';



@Component({
    selector: 'my-app',
    templateUrl: './app/app.component.html',
    directives: [FriendComponent,
                 ClickMeComponent,
                 KeyUpComponent,
                 LoopBackComponent,
                 KeyUp2Component,
                 KeyUp3Component,
                 LittleTourComponent],
    providers: [FriendService]
})


export class AppComponent {

    // constructor(private friends: Array<any>) {}

    title = 'Tour of Heroes';
    heroes = [
        new Hero(1, 'Windstorm'),
        new Hero(2, 'Bombasto'),
        new Hero(3, 'Magneta'),
        new Hero(4, 'Tornado')
    ];
    myHero = this.heroes[0];

    onClickMe() {
        console.log('click!');
    }
}