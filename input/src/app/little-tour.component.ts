import {Component} from '@angular/core';

@Component({
    selector: 'little-tour',
    template: `
        <div class='little-tour-wrap'>
            <input #newHero
                (keyup.enter)='AddHero(newHero.value)'
                (blur)='AddHero(newHero.value); newHero.value=""'>
            <button (click)='AddHero(newHero.value)'>Add</button>
            <ul>
                <li *ngFor='let hero of heroes'>{{hero}}</li>
            </ul>
        </div>
    `,
    styles: [`
        .little-tour-wrap {
            margin: 6px;
        }
    `]
})

export class LittleTourComponent {
    heroes = ['Mr. Bombasto', 'The Tornado', 'The Juggler', 'Rextacular'];

    AddHero(newHero: string) {
        if (newHero) {
            this.heroes.push(newHero);
        }
    }
}