// friend.component

import {Component} from '@angular/core';

import {FriendService} from './friend.service';


@Component({
    selector: 'my-friends',
    template: `
        <h2>Hello from the {{componentName}}!</h2>
        <div *ngFor='let f of friends'
              class='card'>
            <div class='f-name'>{{f.name}}</div> is <div class='f-age'>{{f.age}}</div> years old!
        </div>
    `,
    styles: [`
        .card {
            border: 1px solid black;
            padding: 10px;
            -webkit-border-radius: 10px;
            -moz-border-radius: 10px;
            border-radius: 10px;
        }
        .f-name {
            font-weight: 500;
            font-size: 24px;
            display: inline;
        }
        .f-age {
            font-weight: 200;
            font-style: italic;
            font-size: 16px;
            display: inline;
        }

    `],
    directives: [],
    providers: []
})

export class FriendComponent {
    componentName = 'FriendComponent';
    friends: Array<any>;

    constructor(private _friendService: FriendService) {
        this.friends = _friendService.getFriends();
    }
}