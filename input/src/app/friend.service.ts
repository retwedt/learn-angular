
import {Injectable} from '@angular/core';


@Injectable()
export class FriendService {
    friends: Array<any>;

    constructor() {
        this.friends = [
            {age: 108, name: 'James Artz'},
            {age: 23, name: 'Grace Manuel'},
            {age: 27, name: 'Justin Jacobsen'},
            {age: 28, name: 'Michael Hadley'},
            {age: 36, name: 'Bazooka Washington'}
        ];
    }

    getFriends() {
        return this.friends;
    }
}