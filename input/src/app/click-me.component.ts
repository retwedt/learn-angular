import {Component} from '@angular/core';

@Component({
    selector: 'click-me',
    template: `
        <div class='click-me-wrap'>
            <button (click)='onClickMe()'>Click Me</button>
            {{clickMessage}}
        </div>
    `,
    styles: [`
        button {
            margin: 12px;
        }
        .click-me-wrap {
            margin: 6px;
        }
    `]
})

export class ClickMeComponent {
    clickNum = 0;
    clickMessage = '';

    onClickMe() {
        this.clickNum++;
        this.clickMessage = 'I have been clicked ' + this.clickNum + ' times!';
    }
}