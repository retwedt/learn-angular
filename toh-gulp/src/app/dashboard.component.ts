// import modules from angular core
import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router-deprecated';
// import custom modules
import {Hero} from './hero';
import {HeroService} from './hero.service';

// metadata for this component
@Component({
  selector: 'my-dashboard',
  templateUrl: 'app/dashboard.component.html', // make paths relative to the root folder for the moment, this can be changed later
  styleUrls: ['app/dashboard.component.css']
})

export class DashboardComponent implements OnInit {
  // variables for Dashboard component
  heroes: Hero[] = [];

  // inject private instances into constructor
  constructor(
    private heroService: HeroService,
    private router: Router
  ) {}

  // Angular Lifecycle hooks
  ngOnInit() {
    this.heroService.getHeroes()
      .then(heroes => this.heroes = heroes.slice(1, 5));
  }
  // Public Methods
  gotoDetail(hero: Hero) {
    let link = ['HeroDetail', { id: hero.id }];
    this.router.navigate(link);
  }
}

