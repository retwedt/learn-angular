// hero.service


// import modules from angular core
import {Injectable} from '@angular/core';
import {Headers, Http} from '@angular/http';
// extend the observable object with the RxJS library
import 'rxjs/add/operator/toPromise';
// import custom modules
import {Hero} from './hero';


@Injectable()
export class HeroService {
  // Declare variables 
  private heroesUrl = 'app/heroes'; // base URL to web api

  // attach private instances to the constructor
  constructor(
    private http: Http
  ) {}

  // Public Methods
  // filter the list of heroes by id
  getHero(id: number) {
    return this.getHeroes()
               .then(heroes => heroes.filter(hero => hero.id === id)[0]);
  }
  // use the in-mem web api to fetch data from a mock server
  // get the info for a hero
  getHeroes(): Promise<Hero[]> {
    return this.http
               .get(this.heroesUrl) // Get an object from a remote source
               .toPromise() // convert to a Promise
               .then(response => response.json().data) // collect the data key from the returned object
               .catch(this.handleError); // pass any errors to the handleErrors callback
  }
  // put/post a hero to the db
  save(hero: Hero): Promise<Hero> {
    // if the hero id already exists, use the put method
    if (hero.id) { return this.put(hero); }
    // otherwise create a new entry with the post method
    return this.post(hero);
  }
  // remove a hero from our db
  delete(hero: Hero) {
    let headers = new Headers();
    headers.append('Content-type', 'application/json');

    let url = `${this.heroesUrl}/${hero.id}`;

    return this.http
               .delete(url, headers)
               .toPromise()
               .catch(this.handleError);
  }
  // Private Methods
  // add new hero
  private post(hero: Hero): Promise<Hero> {
    // config the headers for the post request
    let headers = new Headers({
      'Content-Type': 'application/json'
    });
    return this.http
               .post(this.heroesUrl, JSON.stringify(hero), {headers: headers})
               .toPromise()
               .then(res => res.json().data)
               .catch(this.handleError);
  }
  // edit the info on a current hero
  private put(hero: Hero) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    let url = `${this.heroesUrl}/${hero.id}`;

    return this.http
               .put(url, JSON.stringify(hero), {headers:headers})
               .toPromise()
               .then(() => hero) // what is this doing?
               .catch(this.handleError);
  }
  // handle any errors passed to this function
  private handleError(error: any) {
    console.log('an error occured: ', error);
    return Promise.reject(error.message || error);
  }
}
