// hero-detail.component


// import modules from angular
import {Component, EventEmitter, Input, Output, OnInit} from '@angular/core';
import {RouteParams} from '@angular/router-deprecated';
// import custom modules
import {Hero} from './hero';
import {HeroService} from './hero.service';


// metadata for the component decorator
@Component({
	selector: 'my-hero-detail',
	templateUrl: 'app/hero-detail.component.html',
  styleUrls: ['app/hero-detail.component.css']
})


export class HeroDetailComponent implements OnInit {
  // Declare Variables
  @Input() hero: Hero;
  @Output() close = new EventEmitter();
  error: any;
  navigated = false; // set true if navigated to here

  // inject the services into the constructor as private instances
  constructor(
    private heroService: HeroService,
    private routeParams: RouteParams
  ) {}

  // Angular Lifecycle Hooks
  ngOnInit() {
    // if you are trying to edit an existing hero,
    // populate this.hero with the current information
    // otherwise, initialize a new Hero
    if (this.routeParams.get('id') !== null) {
      // params are always strings, so use a '+' to convert the id to an int
      let id = +this.routeParams.get('id');
      this.navigated = true;
      this.heroService
          .getHero(id)
          .then(hero => this.hero = hero);
    } else {
      this.navigated = false;
      this.hero = new Hero();
    }
  }
  // Public methods
  goBack(savedHero: Hero = null) {
    this.close.emit(savedHero);
    if (this.navigated) {
      window.history.back();
    }
  }
  save() {
      this.heroService
          .save(this.hero)
          .then(hero => {
              this.hero = hero; // saved hero w/ id if new
              this.goBack(hero);
          })
          .catch(error => this.error = error); // TODO: display error message
  }
}
