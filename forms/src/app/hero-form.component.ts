// hero-form.component

import {Component} from '@angular/core';
import {NgForm} from '@angular/common';


import {Hero} from './hero';

@Component({
    selector: 'hero-form',
    templateUrl: './app/hero-form.component.html'
})

export class HeroFormComponent {
    powers = ['Super Speed', 'Incredible IQ', 'Brutal Brawler', 'Fascinatingly Flexible'];
    model = new Hero(18, 'Dr IQ', this.powers[0], 'Chuck Overstreet');
    submitted = false;
    active = true;

    onSubmit() {
        this.submitted = true;
    }

    newHero() {
        this.model = new Hero(42, '', '');
        this.active = false;
        setTimeout(() => this.active = true, 0);
    }

    // TODO: Remove this when we are done
    get diagnostic() {
        return JSON.stringify(this.model);
    }
}