# Angular2 Tutorials

These projects were completed while learning Angular2, and experimenting with 
several options for production pipelines.

- [Quickstart](https://angular.io/docs/ts/latest/quickstart.html)
- [Tutorial: Tour of Heroes](https://angular.io/docs/ts/latest/tutorial/)
- [3. Displaying Data](https://angular.io/docs/ts/latest/guide/displaying-data.html)
- [4. User Input](https://angular.io/docs/ts/latest/guide/user-input.html)
- [5. Forms - Deprecated](https://angular.io/docs/ts/latest/guide/forms-deprecated.html)
- [6. Dependency Injection](https://angular.io/docs/ts/latest/guide/dependency-injection.html)
- [Routing & Navigation](https://angular.io/docs/ts/latest/guide/router.html)
- [NgModule](https://angular.io/docs/ts/latest/guide/ngmodule.html)
