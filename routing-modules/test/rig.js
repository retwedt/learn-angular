#!/usr/bin/env node
const jspm = require('jspm')
    , glob = require('glob')

const System = jspm.Loader()

System.config({
  transpiler: 'ts'
})

glob('./test/**/*.ts', (err, files) => {
  if (err)
    return console.error('glob error:', e)

  console.log('test files:', files)

  return Promise.all(files.map((f) => { return System.import(f) }))
    .catch(e => { console.error(e) })
})
