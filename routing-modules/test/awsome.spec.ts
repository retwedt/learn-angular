import test from 'ava'
import 'reflect-metadata'
import 'core-js/fn/object/assign'

import AWSome from 'app/awsome/awsome'
import { AWSSdkObject, AWSCognitoSdkObject } from 'app/awsome/interfaces'


const credentialsMock = {
  accessKeyId: 'akid',
  secretAccessKey: 'shh-secret',
  sessionToken: 'gibberish',
  identityId: 'identity'
}

// TODO(rex): convert to a class
const AWSMock = {
  config: {
    credentials: {
      get: (cb) => {
        Object.assign(AWSome.AWS.config.credentials, credentialsMock, {
          expireTime: new Date()
        })
        cb(credentialsMock)
      }
    }
  },
  CognitoIdentityCredentials: (id) => {}
}

class AWSCognitoMock implements AWSCognitoSdkObject {
  static config = {
    credentials: {
      get: (cb) => {
        Object.assign(AWSome.AWS.config.credentials, credentialsMock, {
          expireTime: new Date()
        })
        cb(credentialsMock)
      }
    }
  }

  static CognitoIdentityServiceProvider = class CognitoIdentityServiceProvider {
    static CognitoUserPool = class CognitoUserPool {
      data : any
      constructor (data) {
        this.data = data
      }
    }

    AuthenticationDetails: (data) => {}
    CognitoUser: (data) => {}
    CognitoUserAttribute: (data) => {}
  }
}


test.beforeEach((t) => {
  AWSome.setAWS(AWSMock)
  AWSome.setAWSCognito(AWSCognitoMock)
})


test('AWSome.credentials', async (t) : Promise<any> => {
  let _credentials = await AWSome.credentials()

  let credentialsNoExpireTime = Object.assign({}, _credentials)
  delete credentialsNoExpireTime.get
  delete credentialsNoExpireTime.expireTime
  
  let get = _credentials.get
  let expireTime = _credentials.expireTime


  t.deepEqual(credentialsNoExpireTime, credentialsMock);

  let testExpireTime = expireTime instanceof Date;
  t.true(testExpireTime)
})


test('AWSome.configure', (t) : void => {
  let testRegion = 'a'
  let testIdentityPoolId = 'b'

  let testConfiguration = {
    region: testRegion,
    credentials: new AWSome.AWS.CognitoIdentityCredentials({IdentityPoolId: testIdentityPoolId})
  }

  let AWSConfig        = Object.assign(AWSMock.config, testConfiguration),
      AWSCognitoConfig = Object.assign(AWSCognitoMock.config, testConfiguration)

  AWSome.configure(testRegion, testIdentityPoolId)

  t.deepEqual(AWSome.AWS.config, AWSConfig)
  t.deepEqual(AWSome.AWSCognito.config, AWSCognitoConfig)
})


test('AWSome.Cognito.CreateUserPool', (t) : void => {
  let testUserPoolId = 'a'
  let testClientId = 'b'

  let testPoolData = {
    UserPoolId: testUserPoolId,
    ClientId: testClientId
  }

  let awsUserPool = AWSome.Cognito.CreateUserPool(testUserPoolId, testClientId)  

  let testUserPool = new AWSCognitoMock.CognitoIdentityServiceProvider.CognitoUserPool(testPoolData)

  t.deepEqual(awsUserPool, testUserPool)
})


// test('AWSome.Cognito.SignIn', async (t) : Promise<any> => {
//   let testUsername = 'a'
//   let testPassword = 'b'
//   let testPool = {
//     UserPoolId: 'c',
//     ClientId: 'd'
//   }

//   let testToken = await AWSome.Cognito.SignIn(testUsername, testPassword, testPool)

//   let awsUser = AWSome.AWSCognito.CognitoIdentityServiceProvider.CognitoUser({Username: testUsername, Pool: testPool})
//   awsUser.authenticateUser()

// })


// test('AWSome.Cognito.SignOut', (t) => {

// })


// test('AWSome.Cognito.ForgotPassword', (t) => {


// })


// test('AWSome.Cognito.RegisterUser', async (t) : Promise<any> => {

// })


// test('AWSome.Cognito.ConfirmRegistration', async (t) : Promise<any> => {

// })


// test('AWSome.Cognito.ResendCode', async (t) : Promise<any> => {
//   let testUsername = 'a'
//   let testUserPoolId = 'b'
//   let testClientId = 'c'

//   let testPoolData = {
//     UserPoolId: testUserPoolId,
//     ClientId: testClientId
//   }

//   let testUserData = {
//     Username: testUsername,
//     Pool: testPoolData
//   }

//   let awsUser = new AWSome.AWSCognito.CognitoIdentityServiceProvider.CognitoUser(testUserData)

//   let testUser = await AWSome.Cognito.ResendCode(testUsername, testPoolData)

// })