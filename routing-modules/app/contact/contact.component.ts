import {
  Component,
  OnInit,
} from '@angular/core'

import { Contact, ContactService } from './contact.service.ts'
import { UserService } from '../shared/user.service.ts'

@Component({
  selector: 'app-contact',
  templateUrl: 'app/contact/contact.component.html',
  styleUrls: ['app/styles/contact.component.css']
})
export class ContactComponent implements OnInit {
  constructor(
    private contactService: ContactService,
    private userService: UserService,
  ) {
    this.userName = userService.userName
  }

  contact: Contact
  contacts: Contact[]

  msg = 'Loading contacts...'
  userName = ''

  ngOnInit() {
    this.contactService.getContacts()
      .then(contacts => {
        this.msg = ''
        this.contacts = contacts
        this.contact = contacts[0]
      })
  }

  next() {
    let ix = 1 + this.contacts.indexOf(this.contact)
    if (ix >= this.contacts.length) ix = 0
    this.contact = this.contacts[ix]
  }

  onSubmit() {
    // TODO(rex): Save this contact or something.
    this.displayMessage('Saved' + this.contact.name)
  }

  newContact() {
    this.displayMessage('New Contact!')
    this.contact = { id: 42, name: '' }
    this.contacts.push(this.contact)
  }

  // NOTE(rex): Briefly display a message, then remove it.
  displayMessage(msg: string) {
    this.msg = msg
    setTimeout(() => {
      this.msg = ''
    }, 1500)
  }
}