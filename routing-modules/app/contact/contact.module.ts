import { NgModule } from '@angular/core'
import { SharedModule } from '../shared/shared.module.ts'

import { ContactComponent } from './contact.component.ts'
import { ContactService } from './contact.service.ts'

import { routing } from './contact.routing.ts'

@NgModule({
  imports: [
    SharedModule,
    routing,
  ],
  declarations: [
    ContactComponent,
  ],
  providers: [
    ContactService,
  ],
})
export class ContactModule { /* */ }