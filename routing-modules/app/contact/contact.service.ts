import { Injectable } from '@angular/core'

// NOTE(rex): Contact Model
export class Contact {
  constructor(
    public id: number,
    public name: string,
  ) {}
}
// NOTE(rex): Dummy data, to simulate a DB
const CONTACTS: Contact[] = [
  new Contact(21, 'Sam Spade'),
  new Contact(22, 'Nick Danger'),
  new Contact(23, 'Nancy Drew'),
  new Contact(28, 'Rex-in-Effect')
]
// NOTE(rex): This is for simulating DB latency
const FETCH_LATENCY = 500

@Injectable()
export class ContactService {

  getContacts() {
    return new Promise<Contact[]>(resolve => {
      setTimeout(() => {
        resolve(CONTACTS)
      }, FETCH_LATENCY)
    })
  }

  getContact(id: number | string) {
    return this.getContacts()
      .then(heroes => heroes.find(
        hero => {
          hero.id === +id
        }
      ))
  }
}