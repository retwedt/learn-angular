import { Component } from '@angular/core'

@Component({
  selector: 'ecm-app',
  templateUrl: 'app/app.component.html',
  styleUrls: ['app/styles/app.css'],
})

export class AppComponent {
  constructor() {}
  title: string = 'Routing and State Management Demo'
}