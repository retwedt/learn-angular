import {
  NgModule,
  ModuleWithProviders,
} from '@angular/core'
import { CommonModule } from '@angular/common'
import { FormsModule } from '@angular/forms'

import { UserService } from './user.service.ts'
import { DialogService } from './dialog.service.ts'


@NgModule({
  imports: [
    CommonModule,
  ],
  declarations: [],
  exports: [
    CommonModule,
    FormsModule
  ],
})
export class SharedModule {
  static forRoot() : ModuleWithProviders {
    return {
      ngModule: SharedModule,
      providers: [
        UserService,
        DialogService
      ]
    }
  }
}