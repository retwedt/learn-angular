import {
  Routes,
  RouterModule,
} from '@angular/router'

import {
  loginRoutes,
  authProviders,
} from './login/login.routing.ts'

import { CanDeactivateGuard } from './shared/can-deactivate-guard.service.ts'

export const crisisCenterRoutes: Routes = [
  { path: '', redirectTo: 'contact', pathMatch: 'full' },
  { path: 'crisis-center', loadChildren: 'app/crisis/crisis-center.module.ts' },
  { path: 'heroes', loadChildren: 'app/hero/hero.module.ts' }
]

const appRoutes: Routes = [
  ...loginRoutes,
  ...crisisCenterRoutes
];

export const appRoutingProviders: any[] = [
  authProviders,
  CanDeactivateGuard
];

export const routing = RouterModule.forRoot(appRoutes)