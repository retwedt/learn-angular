// Polyfills
import 'core-js/fn/object/assign'
// Angular Dependencies
import 'zone.js'
import 'reflect-metadata'

// Angular Components
import { platformBrowserDynamic }    from '@angular/platform-browser-dynamic'
// Custom components
import { AppModule } from './app.module.ts'

// Compile and launch the app
platformBrowserDynamic().bootstrapModule(AppModule)