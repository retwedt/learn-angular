import { NgModule } from '@angular/core'
import { BrowserModule } from '@angular/platform-browser'
import { Routes, RouterModule }   from '@angular/router';

// App Root
import { AppComponent } from './app.component.ts'

// Feature Modules
import { ContactModule } from './contact/contact.module.ts'
import { SharedModule } from './shared/shared.module.ts'

import { LoginComponent } from './login/login.component.ts'

import { routing, appRoutingProviders } from './app.routing.ts'

// NOTE(rex): Needed for Hash location strategy
const routes: Routes = []

@NgModule({
  imports: [
    BrowserModule,
    ContactModule,
    routing,
    SharedModule.forRoot(),
    RouterModule.forRoot(routes, { useHash: true })
  ],
  declarations: [
    AppComponent,
    LoginComponent,
  ],
  providers: [
    appRoutingProviders,
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { /* */ }