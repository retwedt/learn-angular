# An Angular2/Typescript pipeline using Webpack

To use this project:

1. Run `npm i` to install dependencies
2. Run `npm start` to begin compiling typescript, open a local server, and watch files for changes
3. Run `npm run build` to create a production build of your code
4. Run `npm test` to run unit tests using Karma

[Angular2 & Webpack: an Introduction](https://angular.io/docs/ts/latest/guide/webpack.html#!#try)

[Webpack](https://webpack.github.io/)