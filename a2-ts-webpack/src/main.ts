// main

///<reference path="../typings/index.d.ts"/> 

// import core modules
import { bootstrap }    from '@angular/platform-browser-dynamic';
import { enableProdMode } from '@angular/core';
// import custom modules
import { AppComponent } from './app/app.component';

// check for a production flag
if (process.env.ENV === 'production') {
    enableProdMode();
}

// setup app
bootstrap(AppComponent, []);
