
import { Component } from '@angular/core';


import {Hero} from './hero';


@Component({
    selector: 'my-app',
    templateUrl: './app/app.component.html',
    directives: [],
    providers: []
})


export class AppComponent {
    // Declared and initialize properties
    // at the same time
    title = 'Tour of Heroes';
    heroes = [
        new Hero(1, 'Windstorm'),
        new Hero(2, 'The Billiard'),
        new Hero(3, 'Crazy Jack'),
        new Hero(4, 'The Human Mop')
    ];
    myHero = this.heroes[0];
//    myHero = 'Windstorm';



    // This can also be done separately
    // Declare property
    // title: string;
    // myHero: string;

    // Initialize property in constructor
    // constructor() {
    //     this.title = 'Tour of Heroes';
    //     this.myHero = 'Windstorm';
    // }

}