# Displaying Data

This project is from the Angular2 Documentation overview, from the Basics - 3. Displaying Data.

To test this project:

1. Run `npm i` to install dependencies
2. Run `gulp setup` to setup the build folder and copy vendor libs
3. Run `gulp` to build the project, start a local server, and begin monitoring files for changes

[Tutorial Link](https://angular.io/docs/ts/latest/guide/displaying-data.html)
