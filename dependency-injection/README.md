# Dependency Injection

*** This project currently does not work! ***

This project is from the Angular2 Documentation overview, from the Basics - 6. Dependency Injection.

To test this project:

1. Run `npm i` to install dependencies
2. Run `gulp setup` to setup the build folder and copy vendor libs
3. Run `gulp` to build the project, start a local server, and begin monitoring files for changes

[Tutorial Link](https://angular.io/docs/ts/latest/guide/dependency-injection.html)
