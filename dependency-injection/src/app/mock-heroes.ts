// mock data to test our app

import {Hero} from './hero';

export var HEROES: Hero[] = [
    { "id": 11, "name": "Botanica", isSecret: false },
    { "id": 12, "name": "Bombasto", isSecret: false },
    { "id": 13, "name": "Magentar", isSecret: false },
    { "id": 14, "name": "Deliriary", isSecret: false },
    { "id": 15, "name": "The Tornado", isSecret: false },
    { "id": 16, "name": "MineralAnimalVegetable Man", isSecret: false },
    { "id": 17, "name": "The Human Paper Cut", isSecret: false },
    { "id": 18, "name": "The Chasinator", isSecret: true },
    { "id": 19, "name": "The Program", isSecret: true },
    { "id": 20, "name": "Rex", isSecret: true }
];