// hero

// base class for our heroes
export class Hero {
	id: number;
	name: string;
    isSecret = false;
}
