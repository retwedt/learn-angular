// app.component


import {Component} from '@angular/core';

import {HeroesComponent} from './heroes.component';
import {Logger} from './logger.service';


@Component({
  selector: 'my-app',
  templateUrl: 'app/app.component.html',
  directives: [HeroesComponent],
  providers: [Logger]
})


export class AppComponent {
    title = "Rex's Tour of Heroes";
}
