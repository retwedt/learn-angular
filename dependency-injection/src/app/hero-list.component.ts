// hero-detail.component


// import modules from angular
import {Component} from '@angular/core';

// import custom modules
import {Hero} from './hero';
import {HeroService} from './hero.service';


// metadata for the component decorator
@Component({
    selector: 'hero-list',
    template: `
        <div *ngFor="let hero of heroes">
          {{hero.id}} - {{hero.name}}
        </div>
    `
})


export class HeroListComponent {
    heroes: Hero[];

    constructor(heroService: HeroService) {
        this.heroes = heroService.getHeroes();
    }
}
