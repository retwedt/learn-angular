// heroes.component


// import modules from angular core
import {Component} from '@angular/core';

// import custom modules
import {HeroListComponent} from './hero-list.component';
import {HeroService} from './hero.service';


// setup angular component
@Component({
  selector: 'my-heroes',
  template: `
		<h2>Heroes: </h2>
		<hero-list></hero-list>
	`,
	directives: [HeroListComponent],
	providers: [HeroService]
})


export class HeroesComponent {}
