/* Avoid: 'error TS2304: Cannot find name <type>' during compilation */
///<reference path="../../typings/index.d.ts"/>

// Angular2 Dependencies
import { bootstrap }    from '@angular/platform-browser-dynamic';
import { AppComponent } from './app.component';

// Bootstrap our app
bootstrap(AppComponent);
