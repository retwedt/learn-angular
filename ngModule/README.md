# Basic setup for Angular2 development with TypeScript.

### Getting Started

  You need the following global dependencies:

  JSPM 0.17 beta.

  - `npm i -g jspm@beta`

  Typings.

  - `npm i -g typings`

  Stylus.

  - `npm i -g stylus`

  Lite-Server.

  - `npm i -g lite-server`

### Building and Testing

Then you need to install dependencies.

- `npm i && jspm i && typings i`

Run.

- `jspm run app`

Compile and Rollup. (Will create build.js and build.map.js in the project root.)

- `jspm build app`

Compile Stylus stylesheets.

- `npm run styl`

To View:

- `lite-server`

Clean.

- `npm run clean`

Manually compile.

- `tsc -p config`

Manually compile tests.

- `tsc -p test`

Clean up manual compilation artifacts.

- `npm run tsc:clean`

Test.

- `npm test`
