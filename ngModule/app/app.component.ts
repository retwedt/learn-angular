import { Component } from '@angular/core'

@Component({
  selector: 'my-app',
  templateUrl: 'app/app.component.html',
  styleUrls: ['app/styles/app.css'],
})

export class AppComponent {
  constructor() {}

  subtitle: string = '(v1)'
}