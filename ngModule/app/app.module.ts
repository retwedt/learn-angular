import { NgModule } from '@angular/core'
import { BrowserModule } from '@angular/platform-browser'

// App Root
import { AppComponent } from './app.component.ts'

// Feature Modules
import { ContactModule } from './contact/contact.module.ts'
import { SharedModule } from './shared/shared.module.ts'

import { routing } from './app.routing.ts'

@NgModule({
  imports: [
    BrowserModule,
    ContactModule,
    routing,
    SharedModule.forRoot()
  ],
  declarations: [
    AppComponent,
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { /* */ }