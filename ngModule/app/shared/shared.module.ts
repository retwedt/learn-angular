import {
  NgModule,
  ModuleWithProviders,
} from '@angular/core'
import { CommonModule } from '@angular/common'
import { FormsModule } from '@angular/forms'

import { AwesomePipe } from './awesome.pipe.ts'
import { HighlightDirective } from './highlight.directive.ts'
import { UserService } from './user.service.ts'
import { TitleComponent } from './title.component.ts'

@NgModule({
  imports: [
    CommonModule,
  ],
  declarations: [
    AwesomePipe,
    HighlightDirective,
    TitleComponent,
  ],
  exports: [
    AwesomePipe,
    HighlightDirective,
    TitleComponent,
    CommonModule,
    FormsModule
  ],
})
export class SharedModule {
  static forRoot() : ModuleWithProviders {
    return {
      ngModule: SharedModule,
      providers: [ UserService ]
    }
  }
}