import {
  Component,
  Input,
} from '@angular/core'

import { UserService } from './user.service.ts'

@Component({
  selector: 'app-title',
  templateUrl: 'app/shared/title.component.html',
  styleUrls: ['app/styles/app.css'],
})
export class TitleComponent {
  constructor(
    private userService: UserService
  ) {
    this.user = userService.userName
  }

  @Input() subtitle = ''
  title: string = 'NgModule Tutorial'
  user: string = ''
}