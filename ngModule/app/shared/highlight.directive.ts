import {
  Directive,
  ElementRef,
  Renderer,
} from '@angular/core'

@Directive({
  selector: '[highlight], input'
})
export class HighlightDirective {
  constructor(
    private renderer: Renderer,
    private el: ElementRef,
  ) {
    // NOTE(rex): Highlight the attached element in gold
    renderer.setElementStyle(el.nativeElement, 'backgroundColor', 'powderblue')
    console.log(`* Contact highlight called for ${el.nativeElement.tagName}`)
  }
}