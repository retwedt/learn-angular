import test from 'tape'

function fn () : Promise<string> {
  return Promise.resolve('foo')
}

/* NOTE(jordan): async function type annotations are nonsensical.
 *  The annotation is used to determine the Promise implementation, and the generic type is the
 *  actual return type of the function. Which is very strange.
 */
test('example test', async function (t: any) : Promise<void> {
  t.equals(await fn(), 'foo')
  t.end()
})