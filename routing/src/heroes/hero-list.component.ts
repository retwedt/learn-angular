// hero-list.component


// import modules from angular core
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
// import custom modules
import { Hero, HeroService } from './hero.service.ts';


// setup angular component
@Component({
	template: `
		<h2>All Heroes: </h2>
		<ul class="items">
			<li *ngFor="let hero of heroes"
				[class.selected]="isSelected(hero)"
				(click)="onSelect(hero)">
				<span class="badge">{{hero.id}}</span> {{hero.name}}
			</li>
		</ul>
	`
})


// export this HerosComponent class, this will hold
// variables and methods for your app
export class HeroListComponent implements OnInit {
	// declare variables
	heroes: Hero[];

	private selectedId: number;
	private sub: any;

	// bind private instances to the constructor
	constructor(
		private heroService: HeroService,
		private router: Router
	) {}

	// Angular Lifecycle hooks
	ngOnInit() {
		this.sub = this.router
			.routerState
			.queryParams
			.subscribe(params => {
				this.selectedId = +params['id'];
				this.heroService.getHeroes()
					.then(heroes => this.heroes = heroes);
			});
	}
	ngOnDestroy() {
		this.sub.unsubscribe();
	}

	// Public Methods
	onSelect(hero: Hero) {
		this.router.navigate(['/hero', hero.id]);
	}
	isSelected(hero: Hero) {
		return hero.id === this.selectedId;
	}
}
