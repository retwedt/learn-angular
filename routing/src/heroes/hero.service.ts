// hero.service


// import modules from angular core
import { Injectable } from '@angular/core';


// base class for our heroes
export class Hero {
  constructor(
    public id: number,
    public name: string
  ) {}
}

let HEROES = [
  new Hero(11, "Botanica"),
  new Hero(12, "Magentar"),
  new Hero(13, "The Tornado"),
  new Hero(14, "MineralAnimalVegetable Man"),
  new Hero(15, "The Human Program"),
  new Hero(16, "Rex")
];


// setup a promise to mimic getting info from db
let heroesPromise = Promise.resolve(HEROES);


@Injectable()
export class HeroService {
  // Public Methods
  getHeroes() {
    return heroesPromise;
  }

  getHero(id: number | string) {
    return heroesPromise
      .then(heroes => heroes.filter(hero => hero.id === +id)[0]);
  }
}
