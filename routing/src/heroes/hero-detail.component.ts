// hero-detail.component


// import modules from angular
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
// import custom modules
import {Hero, HeroService} from './hero.service.ts';


// metadata for the component decorator
@Component({
	template: `
    <h2 class="subtitle is-2"></h2>
    <div *ngIf="hero">
      <h2>{{hero.name}}</h2>
      <div>
        <label>id: </label>
        {{hero.id}}
      </div>
      <div>
        <label>name: </label>
        <input [(ngModel)]="hero.name" placeholder="name"/>
      </div>
      <button (click)="goToHeroes()">Back</button>
    </div>
  `
})


export class HeroDetailComponent implements OnInit, OnDestroy {
  // Declare Variables
  hero: Hero;

  private sub: any;
  
  // inject the services into the constructor as private instances
  constructor(
    private route: ActivatedRoute,
    private heroService: HeroService,
    private router: Router
  ) {}

  // Angular Lifecycle Hooks

  // *** use this technique to looks at the observable params object, useful
  //     for navigating to multiple instances of the same component without
  //     destroying and recreating the component each time you change views
  ngOnInit() {
    this.sub = this.route
      .params
      .subscribe(params => {
        let id = +params['id']; // converts string 'id' to a number
        this.heroService.getHero(id).then(hero => this.hero = hero);
      });
  }
  ngOnDestroy() {
    // unsubscript to the Observable before the component is destroyed
    // to prevent a memory leak
    this.sub.unsubscribe();
  }

  // *** use this technique if you are going to need a fresh instance of the
  //     component each time you see it (you are not viewing multiple instances
  //     of the same component in a row)
  // ngOnInit() {
  //   // (+) converts string 'id' to a number
  //   let id = +this.route.snapshot.params['id'];
  //   this.heroService.getHero(id).then(hero => this.hero = hero);
  // }


  // Public methods
  goToHeroes() {
    let heroId = this.hero ? this.hero.id : null;
    // pass along hero id if available
    // HeroList component will highlight the selected hero
    this.router.navigate(
      ['/heroes'],
      { queryParams: {id: heroId, foo: 'foo'}}
    );
  }
}
