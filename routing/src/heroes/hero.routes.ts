// hero.routes.ts


/**
 * Import module dependencies
 */
// vendor modules
import { RouterConfig } from '@angular/router';
// custom modules
import { HeroListComponent } from './hero-list.component.ts';
import { HeroDetailComponent } from './hero-detail.component.ts';


/**
 * Export HeroRoutes as an array
 */
export const HeroRoutes: RouterConfig = [
    {path: 'heroes', component: HeroListComponent},
    {path: 'hero/:id', component: HeroDetailComponent}
];