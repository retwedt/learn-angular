// login.routes

import { AuthGuard } from '../auth.guard.ts';
import { AuthService } from '../auth.service.ts';
import { LoginComponent } from './login.component.ts';

export const LoginRoutes = [
    {path: 'login', component: LoginComponent}
];

export const AUTH_PROVIDERS = [
    AuthGuard,
    AuthService
];