// login.component


import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from '../auth.service.ts';


@Component({
    templateUrl: './src/login/login.component.html',
    styleUrls: ['./build/styles/login.component.css']
})


export class LoginComponent {
    message: string;

    constructor(
        public authService: AuthService,
        public router: Router
    ) {}

    // public methods
    setMessage() {
        this.message = 'Logged ' + (this.authService.isLoggedIn ? 'in' : 'out');
    }

    login() {
        this.message = 'Trying to log in...';

        // use the .subscribe method to wait for the response
        // from an observable
        this.authService.login().subscribe(() => {
            this.setMessage();
            if (this.authService.isLoggedIn) {
                this.router.navigate(['/crisis-center/admin']);
            }
        });
    }

    logout() {
        this.authService.logout();
        this.setMessage();
    }
}