// crisis.service


// import modules from angular core
import { Injectable } from '@angular/core';


export class Crisis {
    constructor(
        public id: number,
        public name: string
    ) {}
}

let CRISES = [
    new Crisis(11, 'Enormous Earthquake'),
    new Crisis(12, 'Monstrous Monsoon'),
    new Crisis(13, 'Awful Alien Attack'),
    new Crisis(14, 'Insidious Invasion'),
    new Crisis(15, 'Raucous Robbery')
];


// setup promise to mimic a db
let crisisPromise = Promise.resolve(CRISES);


@Injectable()
export class CrisisService {
    static nextCrisisId = 100;

    // public methods
    getCrises() {
        return crisisPromise;
    }
    getCrisis(id: number | string) {
        return crisisPromise
            .then(crisis => crisis.filter(c => c.id === +id)[0]);
    }
}
