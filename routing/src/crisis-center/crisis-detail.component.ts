// crisis-detail.component


// import modules from angular
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
// *** needed for dealing with observables and promises
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/fromPromise';
// custom modules
import { Crisis, CrisisService } from './crisis.service.ts';
import { DialogService } from '../dialog.service.ts';


// metadata for the component decorator
@Component({
	template: `
        <div *ngIf="crisis">
            <h3>"{{editName}}"</h3>
            <div>
                <label>Id: </label> {{crisis.id}}
            </div>
            <div>
                <label>Name: </label>
                <input [(ngModel)]="editName"
                       placeholder="Name">
            </div>
            <p>
                <button (click)="save()">Save</button>
                <button (click)="cancel()">Cancel</button>
            </p>
        </div>
    `,
    styles: [`
        input {
            width: 100%;
        }
    `]
})


export class CrisisDetailComponent implements OnInit, OnDestroy {
    crisis: Crisis;
    editName: string;
    private sub: any;

    constructor(
        private crisisService: CrisisService,
        private router: Router,
        private route: ActivatedRoute,
        private dialogService: DialogService
    ) {}

    // angular2 lifecycle hooks
    ngOnInit() {
        this.sub = this.route
            .params
            .subscribe(params => {
                let id = +params['id'];
                this.crisisService.getCrisis(id)
                    .then(crisis => {
                        if (crisis) {
                            this.editName = crisis.name;
                            this.crisis = crisis;
                        } else {
                            this.gotoCrises();
                        }
                    });
            });
    }
    ngOnDestory() {
        if (this.sub) {
            this.sub.unsubscribe();
        }
    }

    // public methods
    cancel() {
        this.gotoCrises();
    }

    save() {
        this.crisis.name = this.editName;
        this.gotoCrises();
    }

    canDeactivate(): Observable<boolean> | boolean {
        // allow sync navigation if no crisis or no changes
        if (!this.crisis || this.crisis.name === this.editName) {
            return true;
        }
        // otherwise use a dialog to ask user to save, return a promise that
        // will resolve to true or false when the user makes a selection
        let p = this.dialogService.confirm('Discard Changes?');
        let o = Observable.fromPromise(p);
        return o;
    }

    gotoCrises() {
        let crisisId = this.crisis ? this.crisis.id : null;

        // pass along hero id if available so
        // CrisisListComponent can select that hero
        // also add a `foo` paramater (just because...)
        this.router.navigate(
            [
                '/crisis-center',
                {id: crisisId, foo: 'this is another parameter'}
            ],
            {relativeTo: this.route}
        );
    }
}
