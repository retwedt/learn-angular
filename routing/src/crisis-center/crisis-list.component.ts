// crisis-list.component


// import modules from angular core
import { Component, OnInit, OnDestory } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
// custom modules
import { Crisis, CrisisService } from './crisis.service.ts';


// setup angular component
@Component({
	template: `
        <h2>All Crises: </h2>
        <ul class="items" >
            <li *ngFor="let crisis of crises"
                [class.selected]="isSelected(crisis)"
                (click)="onSelect(crisis)">
                <span class="badge">{{crisis.id}}</span> {{crisis.name}}
            </li>
        </ul>
	`
})


export class CrisisListComponent implements OnInit, OnDestroy {
    crises: Crisis[];
    
    selectedId: number;
    private sub: any;

    constructor(
        private crisisService: CrisisService,
        private route: ActivatedRoute,
        private router: Router
    ) {}

    // angular2 lifecycle hooks
    ngOnInit() {
        this.sub = this.route
            .params
            .subscribe(params => {
                this.selectedId = +params['id'];
                this.crisisService.getCrises()
                    .then(crises => this.crises = crises);
            });
    }
    ngOnDestory() {
        this.sub.unsubscribe();
    }

    // public methods
    onSelect(crisis: Crisis) {
        // absolute link
        this.router.navigate(['/crisis-center', crisis.id]);
    }
    isSelected(crisis: Crisis) {
        return crisis.id === this.selectedId;
    }
}
