// crisis.routes.ts


/**
 * Import module dependencies
 */
// vendor modules
import { RouterConfig } from '@angular/router';
// custom modules
import { CrisisCenterComponent } from './crisis-center.component.ts';
import { CrisisListComponent } from './crisis-list.component.ts';
import { CrisisDetailComponent } from './crisis-detail.component.ts';
import { CrisisAdminComponent } from './crisis-admin.component.ts';

import { CanDeactivateGuard } from '../interfaces.ts';
import { AuthGuard } from '../auth.guard.ts';


/**
 * Export HeroRoutes as an array
 */
export const CrisisCenterRoutes: RouterConfig = [
    {path: '', redirectTo: '/crisis-center', terminal: true},
    {
        path: 'crisis-center',
        component: CrisisCenterComponent,
        children: [
            {path: 'admin', component: CrisisAdminComponent, canActivate: [AuthGuard]},
            {path: ':id', component: CrisisDetailComponent, canDeactivate: [CanDeactivateGuard]},
            {path: '', component: CrisisListComponent}
        ]
    }
];