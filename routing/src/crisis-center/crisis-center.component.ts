// crisis-center.component

// vendor modules
import { Component } from '@angular/core';
import { ROUTER_DIRECTIVES } from '@angular/router';
//custom modules
import { CrisisService } from './crisis.service.ts';

@Component({
    template: `
        <h2 class="subtitle is-2">Crisis Center</h2>    
        <router-outlet></router-outlet>
    `,
    directives: [ROUTER_DIRECTIVES],
    providers: [CrisisService]
})

export class CrisisCenterComponent {}