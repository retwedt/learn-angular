import { Component } from '@angular/core';
import { ROUTER_DIRECTIVES } from '@angular/router';

@Component({
    template: `
        <h3 class="title is-3">CRISIS ADMINISTRATION</h3>
        <p>We're in crisis!</p>
    `,
    directives: [ROUTER_DIRECTIVES]
})

export class CrisisAdminComponent {}