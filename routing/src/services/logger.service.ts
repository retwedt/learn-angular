
import {Injectable} from '@angular/core';

@Injectable()
export class Logger {
    logs: string[] = []; // capture logs for testing

    log(msg: string) {
        this.logs.push(msg);
        console.log(msg);
    }
}