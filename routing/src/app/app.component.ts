// app.component


/**
 * Import module dependencies
 */
 // vendor modules
import { Component } from '@angular/core';
import { ROUTER_DIRECTIVES } from '@angular/router';
// custom modules
import { Logger } from '../services/logger.service.ts';
import { DialogService } from '../dialog.service.ts';
import { HeroService } from '../heroes/hero.service.ts';


/**
 * Component config
 */
@Component({
    selector: 'a2-login',
    templateUrl: './src/app/app.component.html',
    styleUrls: ['./build/styles/app.component.css'],
    directives: [ROUTER_DIRECTIVES],
    providers: [
        Logger,
        HeroService,
        DialogService
    ]
})


/**
 * AppComponent class
 */
export class AppComponent {
    title = 'Routing Test';
}