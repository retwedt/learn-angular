// app.routes


/**
 * Import module dependencies
 */
// vendor modules
import { provideRouter, RouterConfig } from '@angular/router';
// custom modules
import { HeroRoutes } from '../heroes/hero.routes.ts';
import { CrisisCenterRoutes } from '../crisis-center/crisis-center.routes.ts';
import { LoginRoutes, AUTH_PROVIDERS } from '../login/login.routes.ts';
import { CanDeactivateGuard } from '../interfaces.ts';

/**
 * `routes` is an instance of RouterConfig, containing a path and the component
 * to display when that path is navigated to
 */
export const routes = [
    // `...` is the 'spread' operator from ES6
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Spread_operator
    ...HeroRoutes,
    ...LoginRoutes,
    ...CrisisCenterRoutes
];


/**
 * `APP_ROUTER_PROVIDERS` is setup using the provideRouter method, returning a
 * configured 'Router' service provider.  This can be bootstrapped to our
 * application in main.ts
 */
export const APP_ROUTER_PROVIDERS = [
    provideRouter(routes),
    AUTH_PROVIDERS,
    CanDeactivateGuard
];