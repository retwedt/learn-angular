// dialog.service

import { Injectable } from '@angular/core';


@Injectable()
export class DialogService {
    // ask user to confirm an action, 'message' explains the action
    // returns a promise, `true`='confirm', `false`='cancel'
    confirm(message?: string) {
        return new Promise<boolean>(resolve => {
            return resolve(window.confirm(message || 'Is it ok?'));
        });
    }
}