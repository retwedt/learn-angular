// main


/**
 * fix error: `error TS2304: Cannot find name <type>.`
 * from: https://stackoverflow.com/questions/35382157/typescript-build-getting-errors-from-node-modules-folder
 */
///<reference path="../typings/index.d.ts"/> 


/**
 * fix error: `reflect-metadata shim is required when using class decorators`
 * from: https://gist.github.com/robwormald/429e01c6d802767441ec
 */
import 'zone.js';
import 'reflect-metadata';


/**
 * Import module dependencies
 */
 // vendor modules
import { bootstrap }    from '@angular/platform-browser-dynamic';
import { find } from 'ramda';

// custom modules
import { AppComponent } from './app/app.component.ts';
import {APP_ROUTER_PROVIDERS} from './app/app.routes.ts';


// some test code
console.log(find((a) => a == 1)([2, 3, 1]))
console.log("there you are!")


/**
 * Bootstrap application with preconfigured 'Router' service
 */
bootstrap(AppComponent, [
    APP_ROUTER_PROVIDERS
])
.catch(err => console.error(err));