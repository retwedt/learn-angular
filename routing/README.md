# Routing and Navigation

*** This project uses the new Angular2 router, which is in alpha release.  Do not use '@angular/router-deprecated' when completing this project! ***

This project is from the Angular2 Developer Guide, Routing & Navigation.

To test this project:

1. Run `npm i` to install dependencies
2. Run `npm run dev` to build the project, start a local server, and begin monitoring files for changes

[Tutorial Link](https://angular.io/docs/ts/latest/guide/router.html)
