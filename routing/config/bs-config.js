// bs-config.js
"use strict";


// Export config options
module.exports = {
    "files": ["./**/*.html", "./**/*.js", "./**/*.css", "!node_modules", "!./**/*.ts", "!./**/*.styl"],
    "server": {"baseDir": "."},
    "open": "local",
    "https": true,
    "injectChanges": false
};