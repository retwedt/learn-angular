SystemJS.config({
  paths: {
    "github:": "jspm_packages/github/",
    "npm:": "jspm_packages/npm/",
    "app/": "src/"
  },
  browserConfig: {
    "baseURL": "/",
    "bundles": {
      "build/main.js": [
        "app/main.ts",
        "app/app/app.routes.ts",
        "app/interfaces.ts",
        "github:frankwallis/plugin-typescript@4.0.16.json",
        "app/login/login.routes.ts",
        "app/login/login.component.ts",
        "app/auth.service.ts",
        "npm:rxjs@5.0.0-beta.6/add/operator/delay.js",
        "npm:rxjs@5.0.0-beta.6.json",
        "npm:rxjs@5.0.0-beta.6/operator/delay.js",
        "npm:rxjs@5.0.0-beta.6/Notification.js",
        "npm:rxjs@5.0.0-beta.6/Observable.js",
        "npm:rxjs@5.0.0-beta.6/util/toSubscriber.js",
        "npm:rxjs@5.0.0-beta.6/symbol/rxSubscriber.js",
        "npm:rxjs@5.0.0-beta.6/util/root.js",
        "npm:rxjs@5.0.0-beta.6/Subscriber.js",
        "npm:rxjs@5.0.0-beta.6/Observer.js",
        "npm:rxjs@5.0.0-beta.6/Subscription.js",
        "npm:rxjs@5.0.0-beta.6/util/UnsubscriptionError.js",
        "npm:rxjs@5.0.0-beta.6/util/errorObject.js",
        "npm:rxjs@5.0.0-beta.6/util/tryCatch.js",
        "npm:rxjs@5.0.0-beta.6/util/isFunction.js",
        "npm:rxjs@5.0.0-beta.6/util/isObject.js",
        "npm:rxjs@5.0.0-beta.6/util/isArray.js",
        "npm:rxjs@5.0.0-beta.6/symbol/observable.js",
        "npm:rxjs@5.0.0-beta.6/util/isDate.js",
        "npm:rxjs@5.0.0-beta.6/scheduler/async.js",
        "npm:rxjs@5.0.0-beta.6/scheduler/AsyncScheduler.js",
        "npm:rxjs@5.0.0-beta.6/scheduler/QueueScheduler.js",
        "npm:rxjs@5.0.0-beta.6/scheduler/FutureAction.js",
        "npm:rxjs@5.0.0-beta.6/scheduler/QueueAction.js",
        "npm:rxjs@5.0.0-beta.6/add/operator/do.js",
        "npm:rxjs@5.0.0-beta.6/operator/do.js",
        "npm:rxjs@5.0.0-beta.6/add/observable/of.js",
        "npm:rxjs@5.0.0-beta.6/observable/of.js",
        "npm:rxjs@5.0.0-beta.6/observable/ArrayObservable.js",
        "npm:rxjs@5.0.0-beta.6/util/isScheduler.js",
        "npm:rxjs@5.0.0-beta.6/observable/EmptyObservable.js",
        "npm:rxjs@5.0.0-beta.6/observable/ScalarObservable.js",
        "npm:@angular/core@2.0.0-rc.3/index.js",
        "npm:@angular/core@2.0.0-rc.3.json",
        "npm:@angular/core@2.0.0-rc.3/src/animation/animation_player.js",
        "npm:zone.js@0.6.12/dist/zone.js",
        "npm:zone.js@0.6.12.json",
        "github:jspm/nodelibs-process@0.2.0-alpha/process.js",
        "github:jspm/nodelibs-process@0.2.0-alpha.json",
        "npm:reflect-metadata@0.1.3/Reflect.js",
        "npm:reflect-metadata@0.1.3.json",
        "npm:@angular/core@2.0.0-rc.3/src/facade/lang.js",
        "npm:@angular/core@2.0.0-rc.3/src/facade/exceptions.js",
        "npm:@angular/core@2.0.0-rc.3/src/facade/exception_handler.js",
        "npm:@angular/core@2.0.0-rc.3/src/facade/collection.js",
        "npm:@angular/core@2.0.0-rc.3/src/facade/base_wrapped_exception.js",
        "npm:@angular/core@2.0.0-rc.3/src/animation/metadata.js",
        "npm:@angular/core@2.0.0-rc.3/private_export.js",
        "npm:@angular/core@2.0.0-rc.3/src/util/decorators.js",
        "npm:@angular/core@2.0.0-rc.3/src/security.js",
        "npm:@angular/core@2.0.0-rc.3/src/render/api.js",
        "npm:@angular/core@2.0.0-rc.3/src/reflection/reflector_reader.js",
        "npm:@angular/core@2.0.0-rc.3/src/reflection/reflection_capabilities.js",
        "npm:@angular/core@2.0.0-rc.3/src/reflection/reflection.js",
        "npm:@angular/core@2.0.0-rc.3/src/reflection/reflector.js",
        "npm:@angular/core@2.0.0-rc.3/src/profile/wtf_init.js",
        "npm:@angular/core@2.0.0-rc.3/src/metadata/view.js",
        "npm:@angular/core@2.0.0-rc.3/src/metadata/lifecycle_hooks.js",
        "npm:@angular/core@2.0.0-rc.3/src/linker/view_utils.js",
        "npm:@angular/core@2.0.0-rc.3/src/linker/exceptions.js",
        "npm:@angular/core@2.0.0-rc.3/src/linker/element.js",
        "npm:@angular/core@2.0.0-rc.3/src/linker/view_type.js",
        "npm:@angular/core@2.0.0-rc.3/src/linker/view_container_ref.js",
        "npm:@angular/core@2.0.0-rc.3/src/profile/profile.js",
        "npm:@angular/core@2.0.0-rc.3/src/profile/wtf_impl.js",
        "npm:@angular/core@2.0.0-rc.3/src/linker/element_ref.js",
        "npm:@angular/core@2.0.0-rc.3/src/di/decorators.js",
        "npm:@angular/core@2.0.0-rc.3/src/di/metadata.js",
        "npm:@angular/core@2.0.0-rc.3/src/change_detection/change_detection_util.js",
        "npm:@angular/core@2.0.0-rc.3/src/change_detection/change_detection.js",
        "npm:@angular/core@2.0.0-rc.3/src/change_detection/constants.js",
        "npm:@angular/core@2.0.0-rc.3/src/change_detection/change_detector_ref.js",
        "npm:@angular/core@2.0.0-rc.3/src/change_detection/differs/keyvalue_differs.js",
        "npm:@angular/core@2.0.0-rc.3/src/di.js",
        "npm:@angular/core@2.0.0-rc.3/src/di/opaque_token.js",
        "npm:@angular/core@2.0.0-rc.3/src/di/reflective_exceptions.js",
        "npm:@angular/core@2.0.0-rc.3/src/di/reflective_key.js",
        "npm:@angular/core@2.0.0-rc.3/src/di/forward_ref.js",
        "npm:@angular/core@2.0.0-rc.3/src/di/reflective_provider.js",
        "npm:@angular/core@2.0.0-rc.3/src/di/provider_util.js",
        "npm:@angular/core@2.0.0-rc.3/src/di/provider.js",
        "npm:@angular/core@2.0.0-rc.3/src/di/reflective_injector.js",
        "npm:@angular/core@2.0.0-rc.3/src/di/injector.js",
        "npm:@angular/core@2.0.0-rc.3/src/change_detection/differs/iterable_differs.js",
        "npm:@angular/core@2.0.0-rc.3/src/change_detection/differs/default_keyvalue_differ.js",
        "npm:@angular/core@2.0.0-rc.3/src/change_detection/differs/default_iterable_differ.js",
        "npm:@angular/core@2.0.0-rc.3/src/application_tokens.js",
        "npm:@angular/core@2.0.0-rc.3/src/linker/view.js",
        "npm:@angular/core@2.0.0-rc.3/src/animation/active_animation_players_map.js",
        "npm:@angular/core@2.0.0-rc.3/src/animation/animation_group_player.js",
        "npm:@angular/core@2.0.0-rc.3/src/facade/math.js",
        "npm:@angular/core@2.0.0-rc.3/src/linker/element_injector.js",
        "npm:@angular/core@2.0.0-rc.3/src/linker/debug_context.js",
        "npm:@angular/core@2.0.0-rc.3/src/linker/view_ref.js",
        "npm:@angular/core@2.0.0-rc.3/src/facade/async.js",
        "npm:@angular/core@2.0.0-rc.3/src/facade/promise.js",
        "npm:rxjs@5.0.0-beta.6/operator/toPromise.js",
        "npm:rxjs@5.0.0-beta.6/observable/PromiseObservable.js",
        "npm:rxjs@5.0.0-beta.6/Subject.js",
        "npm:rxjs@5.0.0-beta.6/util/ObjectUnsubscribedError.js",
        "npm:rxjs@5.0.0-beta.6/util/throwError.js",
        "npm:rxjs@5.0.0-beta.6/SubjectSubscription.js",
        "npm:@angular/core@2.0.0-rc.3/src/linker/template_ref.js",
        "npm:@angular/core@2.0.0-rc.3/src/linker/component_resolver.js",
        "npm:@angular/core@2.0.0-rc.3/src/linker/component_factory.js",
        "npm:@angular/core@2.0.0-rc.3/src/debug/debug_renderer.js",
        "npm:@angular/core@2.0.0-rc.3/src/debug/debug_node.js",
        "npm:@angular/core@2.0.0-rc.3/src/console.js",
        "npm:@angular/core@2.0.0-rc.3/src/animation/animation_styles.js",
        "npm:@angular/core@2.0.0-rc.3/src/animation/animation_style_util.js",
        "npm:@angular/core@2.0.0-rc.3/src/animation/animation_constants.js",
        "npm:@angular/core@2.0.0-rc.3/src/animation/animation_sequence_player.js",
        "npm:@angular/core@2.0.0-rc.3/src/animation/animation_keyframe.js",
        "npm:@angular/core@2.0.0-rc.3/src/animation/animation_driver.js",
        "npm:@angular/core@2.0.0-rc.3/src/application_common_providers.js",
        "npm:@angular/core@2.0.0-rc.3/src/linker/dynamic_component_loader.js",
        "npm:@angular/core@2.0.0-rc.3/src/application_ref.js",
        "npm:@angular/core@2.0.0-rc.3/src/zone/ng_zone.js",
        "npm:@angular/core@2.0.0-rc.3/src/zone/ng_zone_impl.js",
        "npm:@angular/core@2.0.0-rc.3/src/testability/testability.js",
        "npm:@angular/core@2.0.0-rc.3/src/platform_common_providers.js",
        "npm:@angular/core@2.0.0-rc.3/src/platform_directives_and_pipes.js",
        "npm:@angular/core@2.0.0-rc.3/src/change_detection.js",
        "npm:@angular/core@2.0.0-rc.3/src/linker.js",
        "npm:@angular/core@2.0.0-rc.3/src/linker/systemjs_component_resolver.js",
        "npm:@angular/core@2.0.0-rc.3/src/linker/query_list.js",
        "npm:@angular/core@2.0.0-rc.3/src/render.js",
        "npm:@angular/core@2.0.0-rc.3/src/zone.js",
        "npm:@angular/core@2.0.0-rc.3/src/util.js",
        "npm:@angular/core@2.0.0-rc.3/src/metadata.js",
        "npm:@angular/core@2.0.0-rc.3/src/metadata/directives.js",
        "npm:@angular/core@2.0.0-rc.3/src/metadata/di.js",
        "npm:@angular/router@3.0.0-alpha.7/index.js",
        "npm:@angular/router@3.0.0-alpha.7.json",
        "npm:@angular/router@3.0.0-alpha.7/url_tree.js",
        "npm:@angular/router@3.0.0-alpha.7/utils/collection.js",
        "npm:@angular/router@3.0.0-alpha.7/url_serializer.js",
        "npm:@angular/router@3.0.0-alpha.7/shared.js",
        "npm:@angular/router@3.0.0-alpha.7/router_state.js",
        "npm:@angular/router@3.0.0-alpha.7/utils/tree.js",
        "npm:rxjs@5.0.0-beta.6/BehaviorSubject.js",
        "npm:@angular/router@3.0.0-alpha.7/router_providers.js",
        "npm:@angular/router@3.0.0-alpha.7/common_router_providers.js",
        "npm:@angular/router@3.0.0-alpha.7/router_outlet_map.js",
        "npm:@angular/router@3.0.0-alpha.7/router.js",
        "npm:@angular/router@3.0.0-alpha.7/resolve.js",
        "npm:rxjs@5.0.0-beta.6/observable/fromPromise.js",
        "npm:rxjs@5.0.0-beta.6/observable/forkJoin.js",
        "npm:rxjs@5.0.0-beta.6/observable/ForkJoinObservable.js",
        "npm:rxjs@5.0.0-beta.6/OuterSubscriber.js",
        "npm:rxjs@5.0.0-beta.6/util/subscribeToResult.js",
        "npm:rxjs@5.0.0-beta.6/InnerSubscriber.js",
        "npm:rxjs@5.0.0-beta.6/symbol/iterator.js",
        "npm:rxjs@5.0.0-beta.6/util/isPromise.js",
        "npm:rxjs@5.0.0-beta.6/add/operator/toPromise.js",
        "npm:rxjs@5.0.0-beta.6/add/operator/map.js",
        "npm:rxjs@5.0.0-beta.6/operator/map.js",
        "npm:@angular/router@3.0.0-alpha.7/recognize.js",
        "npm:@angular/router@3.0.0-alpha.7/create_url_tree.js",
        "npm:@angular/router@3.0.0-alpha.7/create_router_state.js",
        "npm:@angular/router@3.0.0-alpha.7/config.js",
        "npm:@angular/router@3.0.0-alpha.7/apply_redirects.js",
        "npm:rxjs@5.0.0-beta.6/add/observable/from.js",
        "npm:rxjs@5.0.0-beta.6/observable/from.js",
        "npm:rxjs@5.0.0-beta.6/observable/FromObservable.js",
        "npm:rxjs@5.0.0-beta.6/operator/observeOn.js",
        "npm:rxjs@5.0.0-beta.6/observable/ArrayLikeObservable.js",
        "npm:rxjs@5.0.0-beta.6/observable/IteratorObservable.js",
        "npm:rxjs@5.0.0-beta.6/add/operator/mergeAll.js",
        "npm:rxjs@5.0.0-beta.6/operator/mergeAll.js",
        "npm:rxjs@5.0.0-beta.6/add/operator/every.js",
        "npm:rxjs@5.0.0-beta.6/operator/every.js",
        "npm:rxjs@5.0.0-beta.6/add/operator/concatMap.js",
        "npm:rxjs@5.0.0-beta.6/operator/concatMap.js",
        "npm:rxjs@5.0.0-beta.6/operator/mergeMap.js",
        "npm:rxjs@5.0.0-beta.6/add/operator/concat.js",
        "npm:rxjs@5.0.0-beta.6/operator/concat.js",
        "npm:rxjs@5.0.0-beta.6/add/operator/mergeMap.js",
        "npm:rxjs@5.0.0-beta.6/add/operator/scan.js",
        "npm:rxjs@5.0.0-beta.6/operator/scan.js",
        "npm:@angular/common@2.0.0-rc.3/index.js",
        "npm:@angular/common@2.0.0-rc.3.json",
        "npm:@angular/common@2.0.0-rc.3/src/location.js",
        "npm:@angular/common@2.0.0-rc.3/src/location/location.js",
        "npm:@angular/common@2.0.0-rc.3/src/location/location_strategy.js",
        "npm:@angular/common@2.0.0-rc.3/src/facade/async.js",
        "npm:@angular/common@2.0.0-rc.3/src/facade/promise.js",
        "npm:@angular/common@2.0.0-rc.3/src/facade/lang.js",
        "npm:@angular/common@2.0.0-rc.3/src/location/path_location_strategy.js",
        "npm:@angular/common@2.0.0-rc.3/src/location/platform_location.js",
        "npm:@angular/common@2.0.0-rc.3/src/facade/exceptions.js",
        "npm:@angular/common@2.0.0-rc.3/src/facade/exception_handler.js",
        "npm:@angular/common@2.0.0-rc.3/src/facade/collection.js",
        "npm:@angular/common@2.0.0-rc.3/src/facade/base_wrapped_exception.js",
        "npm:@angular/common@2.0.0-rc.3/src/location/hash_location_strategy.js",
        "npm:@angular/common@2.0.0-rc.3/src/common_directives.js",
        "npm:@angular/common@2.0.0-rc.3/src/forms-deprecated.js",
        "npm:@angular/common@2.0.0-rc.3/src/forms-deprecated/validators.js",
        "npm:@angular/common@2.0.0-rc.3/src/forms-deprecated/model.js",
        "npm:@angular/common@2.0.0-rc.3/src/forms-deprecated/directives/validators.js",
        "npm:@angular/common@2.0.0-rc.3/src/forms-deprecated/directives/select_control_value_accessor.js",
        "npm:@angular/common@2.0.0-rc.3/src/forms-deprecated/directives/control_value_accessor.js",
        "npm:@angular/common@2.0.0-rc.3/src/forms-deprecated/directives/ng_model.js",
        "npm:@angular/common@2.0.0-rc.3/src/forms-deprecated/directives/shared.js",
        "npm:@angular/common@2.0.0-rc.3/src/forms-deprecated/directives/radio_control_value_accessor.js",
        "npm:@angular/common@2.0.0-rc.3/src/forms-deprecated/directives/ng_control.js",
        "npm:@angular/common@2.0.0-rc.3/src/forms-deprecated/directives/abstract_control_directive.js",
        "npm:@angular/common@2.0.0-rc.3/src/forms-deprecated/directives/number_value_accessor.js",
        "npm:@angular/common@2.0.0-rc.3/src/forms-deprecated/directives/normalize_validator.js",
        "npm:@angular/common@2.0.0-rc.3/src/forms-deprecated/directives/default_value_accessor.js",
        "npm:@angular/common@2.0.0-rc.3/src/forms-deprecated/directives/checkbox_value_accessor.js",
        "npm:@angular/common@2.0.0-rc.3/src/forms-deprecated/directives/ng_form_model.js",
        "npm:@angular/common@2.0.0-rc.3/src/forms-deprecated/directives/control_container.js",
        "npm:@angular/common@2.0.0-rc.3/src/forms-deprecated/directives/ng_form_control.js",
        "npm:@angular/common@2.0.0-rc.3/src/forms-deprecated/directives/ng_form.js",
        "npm:@angular/common@2.0.0-rc.3/src/forms-deprecated/directives/ng_control_status.js",
        "npm:@angular/common@2.0.0-rc.3/src/forms-deprecated/directives/ng_control_name.js",
        "npm:@angular/common@2.0.0-rc.3/src/forms-deprecated/directives/ng_control_group.js",
        "npm:@angular/common@2.0.0-rc.3/src/forms-deprecated/directives.js",
        "npm:@angular/common@2.0.0-rc.3/src/forms-deprecated/directives/select_multiple_control_value_accessor.js",
        "npm:@angular/common@2.0.0-rc.3/src/forms-deprecated/form_builder.js",
        "npm:@angular/common@2.0.0-rc.3/src/directives.js",
        "npm:@angular/common@2.0.0-rc.3/src/directives/ng_template_outlet.js",
        "npm:@angular/common@2.0.0-rc.3/src/directives/ng_switch.js",
        "npm:@angular/common@2.0.0-rc.3/src/directives/ng_style.js",
        "npm:@angular/common@2.0.0-rc.3/src/directives/ng_plural.js",
        "npm:@angular/common@2.0.0-rc.3/src/directives/ng_if.js",
        "npm:@angular/common@2.0.0-rc.3/src/directives/ng_for.js",
        "npm:@angular/common@2.0.0-rc.3/src/directives/ng_class.js",
        "npm:@angular/common@2.0.0-rc.3/src/directives/core_directives.js",
        "npm:@angular/common@2.0.0-rc.3/src/pipes.js",
        "npm:@angular/common@2.0.0-rc.3/src/pipes/uppercase_pipe.js",
        "npm:@angular/common@2.0.0-rc.3/src/pipes/invalid_pipe_argument_exception.js",
        "npm:@angular/common@2.0.0-rc.3/src/pipes/slice_pipe.js",
        "npm:@angular/common@2.0.0-rc.3/src/pipes/replace_pipe.js",
        "npm:@angular/common@2.0.0-rc.3/src/pipes/number_pipe.js",
        "npm:@angular/common@2.0.0-rc.3/src/facade/intl.js",
        "npm:@angular/common@2.0.0-rc.3/src/pipes/lowercase_pipe.js",
        "npm:@angular/common@2.0.0-rc.3/src/pipes/json_pipe.js",
        "npm:@angular/common@2.0.0-rc.3/src/pipes/i18n_select_pipe.js",
        "npm:@angular/common@2.0.0-rc.3/src/pipes/i18n_plural_pipe.js",
        "npm:@angular/common@2.0.0-rc.3/src/pipes/date_pipe.js",
        "npm:@angular/common@2.0.0-rc.3/src/pipes/common_pipes.js",
        "npm:@angular/common@2.0.0-rc.3/src/pipes/async_pipe.js",
        "npm:@angular/platform-browser@2.0.0-rc.3/index.js",
        "npm:@angular/platform-browser@2.0.0-rc.3.json",
        "npm:@angular/platform-browser@2.0.0-rc.3/private_export.js",
        "npm:@angular/platform-browser@2.0.0-rc.3/src/dom/shared_styles_host.js",
        "npm:@angular/platform-browser@2.0.0-rc.3/src/dom/dom_tokens.js",
        "npm:@angular/platform-browser@2.0.0-rc.3/src/dom/dom_adapter.js",
        "npm:@angular/platform-browser@2.0.0-rc.3/src/facade/lang.js",
        "npm:@angular/platform-browser@2.0.0-rc.3/src/facade/collection.js",
        "npm:@angular/platform-browser@2.0.0-rc.3/src/dom/dom_renderer.js",
        "npm:@angular/platform-browser@2.0.0-rc.3/src/dom/util.js",
        "npm:@angular/platform-browser@2.0.0-rc.3/src/dom/events/event_manager.js",
        "npm:@angular/platform-browser@2.0.0-rc.3/src/facade/exceptions.js",
        "npm:@angular/platform-browser@2.0.0-rc.3/src/facade/exception_handler.js",
        "npm:@angular/platform-browser@2.0.0-rc.3/src/facade/base_wrapped_exception.js",
        "npm:@angular/platform-browser@2.0.0-rc.3/core_private.js",
        "npm:@angular/platform-browser@2.0.0-rc.3/src/worker_app.js",
        "npm:@angular/platform-browser@2.0.0-rc.3/src/web_workers/worker/worker_adapter.js",
        "npm:@angular/platform-browser@2.0.0-rc.3/src/web_workers/worker/renderer.js",
        "npm:@angular/platform-browser@2.0.0-rc.3/src/web_workers/worker/event_deserializer.js",
        "npm:@angular/platform-browser@2.0.0-rc.3/src/web_workers/shared/serializer.js",
        "npm:@angular/platform-browser@2.0.0-rc.3/src/web_workers/shared/serialized_types.js",
        "npm:@angular/platform-browser@2.0.0-rc.3/src/web_workers/shared/render_store.js",
        "npm:@angular/platform-browser@2.0.0-rc.3/src/web_workers/shared/messaging_api.js",
        "npm:@angular/platform-browser@2.0.0-rc.3/src/web_workers/shared/message_bus.js",
        "npm:@angular/platform-browser@2.0.0-rc.3/src/web_workers/shared/client_message_broker.js",
        "npm:@angular/platform-browser@2.0.0-rc.3/src/facade/async.js",
        "npm:@angular/platform-browser@2.0.0-rc.3/src/facade/promise.js",
        "npm:@angular/platform-browser@2.0.0-rc.3/src/web_workers/shared/service_message_broker.js",
        "npm:@angular/platform-browser@2.0.0-rc.3/src/web_workers/shared/post_message_bus.js",
        "npm:@angular/platform-browser@2.0.0-rc.3/src/web_workers/shared/api.js",
        "npm:@angular/platform-browser@2.0.0-rc.3/src/browser.js",
        "npm:@angular/platform-browser@2.0.0-rc.3/src/security/dom_sanitization_service.js",
        "npm:@angular/platform-browser@2.0.0-rc.3/src/security/url_sanitizer.js",
        "npm:@angular/platform-browser@2.0.0-rc.3/src/security/style_sanitizer.js",
        "npm:@angular/platform-browser@2.0.0-rc.3/src/security/html_sanitizer.js",
        "npm:@angular/platform-browser@2.0.0-rc.3/src/dom/events/key_events.js",
        "npm:@angular/platform-browser@2.0.0-rc.3/src/dom/events/hammer_gestures.js",
        "npm:@angular/platform-browser@2.0.0-rc.3/src/dom/events/hammer_common.js",
        "npm:@angular/platform-browser@2.0.0-rc.3/src/dom/events/dom_events.js",
        "npm:@angular/platform-browser@2.0.0-rc.3/src/dom/debug/ng_probe.js",
        "npm:@angular/platform-browser@2.0.0-rc.3/src/browser/testability.js",
        "npm:@angular/platform-browser@2.0.0-rc.3/src/browser/location/browser_platform_location.js",
        "npm:@angular/platform-browser@2.0.0-rc.3/src/browser/location/history.js",
        "npm:@angular/platform-browser@2.0.0-rc.3/src/browser/browser_adapter.js",
        "npm:@angular/platform-browser@2.0.0-rc.3/src/browser/generic_browser_adapter.js",
        "npm:@angular/platform-browser@2.0.0-rc.3/src/dom/web_animations_driver.js",
        "npm:@angular/platform-browser@2.0.0-rc.3/src/dom/web_animations_player.js",
        "npm:@angular/platform-browser@2.0.0-rc.3/src/worker_render.js",
        "npm:@angular/platform-browser@2.0.0-rc.3/src/web_workers/ui/renderer.js",
        "npm:@angular/platform-browser@2.0.0-rc.3/src/web_workers/ui/event_dispatcher.js",
        "npm:@angular/platform-browser@2.0.0-rc.3/src/web_workers/ui/event_serializer.js",
        "npm:@angular/platform-browser@2.0.0-rc.3/src/web_workers/ui/location_providers.js",
        "npm:@angular/platform-browser@2.0.0-rc.3/src/web_workers/ui/platform_location.js",
        "npm:@angular/platform-browser@2.0.0-rc.3/src/web_workers/worker/location_providers.js",
        "npm:@angular/platform-browser@2.0.0-rc.3/src/web_workers/worker/platform_location.js",
        "npm:@angular/platform-browser@2.0.0-rc.3/src/dom/debug/by.js",
        "npm:@angular/platform-browser@2.0.0-rc.3/src/browser/tools/tools.js",
        "npm:@angular/platform-browser@2.0.0-rc.3/src/browser/tools/common_tools.js",
        "npm:@angular/platform-browser@2.0.0-rc.3/src/facade/browser.js",
        "npm:@angular/platform-browser@2.0.0-rc.3/src/browser/title.js",
        "npm:@angular/router@3.0.0-alpha.7/directives/router_outlet.js",
        "npm:@angular/router@3.0.0-alpha.7/directives/router_link_active.js",
        "npm:@angular/router@3.0.0-alpha.7/directives/router_link.js",
        "app/auth.guard.ts",
        "app/crisis-center/crisis-center.routes.ts",
        "app/crisis-center/crisis-admin.component.ts",
        "app/crisis-center/crisis-detail.component.ts",
        "app/dialog.service.ts",
        "app/crisis-center/crisis.service.ts",
        "npm:rxjs@5.0.0-beta.6/add/observable/fromPromise.js",
        "app/crisis-center/crisis-list.component.ts",
        "app/crisis-center/crisis-center.component.ts",
        "app/heroes/hero.routes.ts",
        "app/heroes/hero-detail.component.ts",
        "app/heroes/hero.service.ts",
        "app/heroes/hero-list.component.ts",
        "app/app/app.component.ts",
        "app/services/logger.service.ts",
        "npm:ramda@0.21.0/dist/ramda.js",
        "npm:ramda@0.21.0.json",
        "npm:@angular/platform-browser-dynamic@2.0.0-rc.3/index.js",
        "npm:@angular/platform-browser-dynamic@2.0.0-rc.3.json",
        "npm:@angular/platform-browser-dynamic@2.0.0-rc.3/src/xhr/xhr_impl.js",
        "npm:@angular/platform-browser-dynamic@2.0.0-rc.3/src/facade/promise.js",
        "npm:@angular/platform-browser-dynamic@2.0.0-rc.3/src/facade/lang.js",
        "npm:@angular/compiler@2.0.0-rc.3/index.js",
        "npm:@angular/compiler@2.0.0-rc.3.json",
        "npm:@angular/compiler@2.0.0-rc.3/compiler.js",
        "npm:@angular/compiler@2.0.0-rc.3/private_export.js",
        "npm:@angular/compiler@2.0.0-rc.3/src/view_compiler/view_compiler.js",
        "npm:@angular/compiler@2.0.0-rc.3/src/view_compiler/view_builder.js",
        "npm:@angular/compiler@2.0.0-rc.3/src/animation/animation_compiler.js",
        "npm:@angular/compiler@2.0.0-rc.3/src/animation/animation_parser.js",
        "npm:@angular/compiler@2.0.0-rc.3/src/animation/styles_collection.js",
        "npm:@angular/compiler@2.0.0-rc.3/src/facade/lang.js",
        "npm:@angular/compiler@2.0.0-rc.3/src/facade/collection.js",
        "npm:@angular/compiler@2.0.0-rc.3/src/animation/animation_ast.js",
        "npm:@angular/compiler@2.0.0-rc.3/src/parse_util.js",
        "npm:@angular/compiler@2.0.0-rc.3/src/facade/math.js",
        "npm:@angular/compiler@2.0.0-rc.3/src/compile_metadata.js",
        "npm:@angular/compiler@2.0.0-rc.3/src/util.js",
        "npm:@angular/compiler@2.0.0-rc.3/src/url_resolver.js",
        "npm:@angular/compiler@2.0.0-rc.3/src/selector.js",
        "npm:@angular/compiler@2.0.0-rc.3/src/facade/exceptions.js",
        "npm:@angular/compiler@2.0.0-rc.3/src/facade/exception_handler.js",
        "npm:@angular/compiler@2.0.0-rc.3/src/facade/base_wrapped_exception.js",
        "npm:@angular/compiler@2.0.0-rc.3/core_private.js",
        "npm:@angular/compiler@2.0.0-rc.3/src/output/output_ast.js",
        "npm:@angular/compiler@2.0.0-rc.3/src/identifiers.js",
        "npm:@angular/compiler@2.0.0-rc.3/src/view_compiler/util.js",
        "npm:@angular/compiler@2.0.0-rc.3/src/template_ast.js",
        "npm:@angular/compiler@2.0.0-rc.3/src/view_compiler/constants.js",
        "npm:@angular/compiler@2.0.0-rc.3/src/view_compiler/compile_view.js",
        "npm:@angular/compiler@2.0.0-rc.3/src/view_compiler/compile_query.js",
        "npm:@angular/compiler@2.0.0-rc.3/src/view_compiler/compile_pipe.js",
        "npm:@angular/compiler@2.0.0-rc.3/src/view_compiler/compile_method.js",
        "npm:@angular/compiler@2.0.0-rc.3/src/view_compiler/compile_element.js",
        "npm:@angular/compiler@2.0.0-rc.3/src/view_compiler/view_binder.js",
        "npm:@angular/compiler@2.0.0-rc.3/src/view_compiler/lifecycle_binder.js",
        "npm:@angular/compiler@2.0.0-rc.3/src/view_compiler/event_binder.js",
        "npm:@angular/compiler@2.0.0-rc.3/src/view_compiler/expression_converter.js",
        "npm:@angular/compiler@2.0.0-rc.3/src/view_compiler/compile_binding.js",
        "npm:@angular/compiler@2.0.0-rc.3/src/view_compiler/property_binder.js",
        "npm:@angular/compiler@2.0.0-rc.3/src/config.js",
        "npm:@angular/compiler@2.0.0-rc.3/src/template_parser.js",
        "npm:@angular/compiler@2.0.0-rc.3/src/provider_parser.js",
        "npm:@angular/compiler@2.0.0-rc.3/src/html_ast.js",
        "npm:@angular/compiler@2.0.0-rc.3/src/style_url_resolver.js",
        "npm:@angular/compiler@2.0.0-rc.3/src/template_preparser.js",
        "npm:@angular/compiler@2.0.0-rc.3/src/html_tags.js",
        "npm:@angular/compiler@2.0.0-rc.3/src/schema/element_schema_registry.js",
        "npm:@angular/compiler@2.0.0-rc.3/src/html_parser.js",
        "npm:@angular/compiler@2.0.0-rc.3/src/html_lexer.js",
        "npm:@angular/compiler@2.0.0-rc.3/src/interpolation_config.js",
        "npm:@angular/compiler@2.0.0-rc.3/src/chars.js",
        "npm:@angular/compiler@2.0.0-rc.3/src/expression_parser/parser.js",
        "npm:@angular/compiler@2.0.0-rc.3/src/expression_parser/lexer.js",
        "npm:@angular/compiler@2.0.0-rc.3/src/expression_parser/ast.js",
        "npm:@angular/compiler@2.0.0-rc.3/src/style_compiler.js",
        "npm:@angular/compiler@2.0.0-rc.3/src/shadow_css.js",
        "npm:@angular/compiler@2.0.0-rc.3/src/schema/dom_element_schema_registry.js",
        "npm:@angular/compiler@2.0.0-rc.3/src/schema/dom_security_schema.js",
        "npm:@angular/compiler@2.0.0-rc.3/src/output/ts_emitter.js",
        "npm:@angular/compiler@2.0.0-rc.3/src/output/abstract_emitter.js",
        "npm:@angular/compiler@2.0.0-rc.3/src/output/path_util.js",
        "npm:@angular/compiler@2.0.0-rc.3/src/metadata_resolver.js",
        "npm:@angular/compiler@2.0.0-rc.3/src/view_resolver.js",
        "npm:@angular/compiler@2.0.0-rc.3/src/pipe_resolver.js",
        "npm:@angular/compiler@2.0.0-rc.3/src/directive_resolver.js",
        "npm:@angular/compiler@2.0.0-rc.3/src/directive_lifecycle_reflector.js",
        "npm:@angular/compiler@2.0.0-rc.3/src/assertions.js",
        "npm:@angular/compiler@2.0.0-rc.3/src/i18n/xmb_serializer.js",
        "npm:@angular/compiler@2.0.0-rc.3/src/i18n/message.js",
        "npm:@angular/compiler@2.0.0-rc.3/src/i18n/message_extractor.js",
        "npm:@angular/compiler@2.0.0-rc.3/src/i18n/shared.js",
        "npm:@angular/compiler@2.0.0-rc.3/src/i18n/i18n_html_parser.js",
        "npm:@angular/compiler@2.0.0-rc.3/src/i18n/expander.js",
        "npm:@angular/compiler@2.0.0-rc.3/src/directive_normalizer.js",
        "npm:@angular/compiler@2.0.0-rc.3/src/xhr.js",
        "npm:@angular/compiler@2.0.0-rc.3/src/facade/async.js",
        "npm:@angular/compiler@2.0.0-rc.3/src/facade/promise.js",
        "npm:@angular/compiler@2.0.0-rc.3/src/compiler.js",
        "npm:@angular/compiler@2.0.0-rc.3/src/runtime_compiler.js",
        "npm:@angular/compiler@2.0.0-rc.3/src/output/interpretive_view.js",
        "npm:@angular/compiler@2.0.0-rc.3/src/output/output_interpreter.js",
        "npm:@angular/compiler@2.0.0-rc.3/src/output/dart_emitter.js",
        "npm:@angular/compiler@2.0.0-rc.3/src/output/output_jit.js",
        "npm:@angular/compiler@2.0.0-rc.3/src/output/abstract_js_emitter.js",
        "npm:@angular/compiler@2.0.0-rc.3/src/offline_compiler.js",
        "npm:@angular/platform-browser-dynamic@2.0.0-rc.3/src/xhr/xhr_cache.js",
        "npm:@angular/platform-browser-dynamic@2.0.0-rc.3/src/facade/exceptions.js",
        "npm:@angular/platform-browser-dynamic@2.0.0-rc.3/src/facade/exception_handler.js",
        "npm:@angular/platform-browser-dynamic@2.0.0-rc.3/src/facade/collection.js",
        "npm:@angular/platform-browser-dynamic@2.0.0-rc.3/src/facade/base_wrapped_exception.js",
        "npm:@angular/platform-browser-dynamic@2.0.0-rc.3/src/facade/async.js",
        "npm:@angular/platform-browser-dynamic@2.0.0-rc.3/core_private.js"
      ]
    }
  },
  devConfig: {
    "map": {
      "plugin-typescript": "github:frankwallis/plugin-typescript@4.0.16"
    }
  },
  transpiler: "plugin-typescript",
  packages: {
    "app": {
      "main": "main.ts",
      "format": "system",
      "meta": {
        "*.ts": {
          "loader": "plugin-typescript"
        }
      }
    }
  },
  typescriptOptions: {
    "tsconfig": "config/tsconfig.json",
    "module": "system"
  }
});

SystemJS.config({
  packageConfigPaths: [
    "github:*/*.json",
    "npm:@*/*.json",
    "npm:*.json"
  ],
  map: {
    "@angular/common": "npm:@angular/common@2.0.0-rc.3",
    "@angular/compiler": "npm:@angular/compiler@2.0.0-rc.3",
    "@angular/core": "npm:@angular/core@2.0.0-rc.3",
    "@angular/http": "npm:@angular/http@2.0.0-rc.3",
    "@angular/platform-browser": "npm:@angular/platform-browser@2.0.0-rc.3",
    "@angular/platform-browser-dynamic": "npm:@angular/platform-browser-dynamic@2.0.0-rc.3",
    "@angular/router": "npm:@angular/router@3.0.0-alpha.7",
    "@angular/router-deprecated": "npm:@angular/router-deprecated@2.0.0-rc.2",
    "@angular/upgrade": "npm:@angular/upgrade@2.0.0-rc.3",
    "assert": "github:jspm/nodelibs-assert@0.2.0-alpha",
    "buffer": "github:jspm/nodelibs-buffer@0.2.0-alpha",
    "child_process": "github:jspm/nodelibs-child_process@0.2.0-alpha",
    "cluster": "github:jspm/nodelibs-cluster@0.2.0-alpha",
    "constants": "github:jspm/nodelibs-constants@0.2.0-alpha",
    "crypto": "github:jspm/nodelibs-crypto@0.2.0-alpha",
    "dgram": "github:jspm/nodelibs-dgram@0.2.0-alpha",
    "dns": "github:jspm/nodelibs-dns@0.2.0-alpha",
    "domain": "github:jspm/nodelibs-domain@0.2.0-alpha",
    "ecc-jsbn": "npm:ecc-jsbn@0.1.1",
    "es6-shim": "github:es-shims/es6-shim@0.35.1",
    "events": "github:jspm/nodelibs-events@0.2.0-alpha",
    "frankwallis/plugin-typescript": "github:frankwallis/plugin-typescript@4.0.16",
    "fs": "github:jspm/nodelibs-fs@0.2.0-alpha",
    "fsevents": "npm:fsevents@1.0.12",
    "http": "github:jspm/nodelibs-http@0.2.0-alpha",
    "https": "github:jspm/nodelibs-https@0.2.0-alpha",
    "jodid25519": "npm:jodid25519@1.0.2",
    "jsbn": "npm:jsbn@0.1.0",
    "module": "github:jspm/nodelibs-module@0.2.0-alpha",
    "net": "github:jspm/nodelibs-net@0.2.0-alpha",
    "os": "github:jspm/nodelibs-os@0.2.0-alpha",
    "path": "github:jspm/nodelibs-path@0.2.0-alpha",
    "process": "github:jspm/nodelibs-process@0.2.0-alpha",
    "punycode": "github:jspm/nodelibs-punycode@0.2.0-alpha",
    "querystring": "github:jspm/nodelibs-querystring@0.2.0-alpha",
    "ramda": "npm:ramda@0.21.0",
    "readline": "github:jspm/nodelibs-readline@0.2.0-alpha",
    "reflect-metadata": "npm:reflect-metadata@0.1.3",
    "repl": "github:jspm/nodelibs-repl@0.2.0-alpha",
    "rxjs": "npm:rxjs@5.0.0-beta.6",
    "stream": "github:jspm/nodelibs-stream@0.2.0-alpha",
    "string_decoder": "github:jspm/nodelibs-string_decoder@0.2.0-alpha",
    "tls": "github:jspm/nodelibs-tls@0.2.0-alpha",
    "tty": "github:jspm/nodelibs-tty@0.2.0-alpha",
    "tweetnacl": "npm:tweetnacl@0.13.3",
    "typescript": "npm:typescript@1.8.10",
    "url": "github:jspm/nodelibs-url@0.2.0-alpha",
    "util": "github:jspm/nodelibs-util@0.2.0-alpha",
    "vm": "github:jspm/nodelibs-vm@0.2.0-alpha",
    "zlib": "github:jspm/nodelibs-zlib@0.2.0-alpha",
    "zone.js": "npm:zone.js@0.6.12"
  },
  packages: {
    "github:frankwallis/plugin-typescript@4.0.16": {
      "map": {
        "typescript": "npm:typescript@1.8.10"
      }
    },
    "github:jspm/nodelibs-os@0.2.0-alpha": {
      "map": {
        "os-browserify": "npm:os-browserify@0.2.1"
      }
    },
    "npm:chalk@1.1.3": {
      "map": {
        "strip-ansi": "npm:strip-ansi@3.0.1",
        "escape-string-regexp": "npm:escape-string-regexp@1.0.5",
        "supports-color": "npm:supports-color@2.0.0",
        "ansi-styles": "npm:ansi-styles@2.2.1",
        "has-ansi": "npm:has-ansi@2.0.0"
      }
    },
    "npm:debug@2.2.0": {
      "map": {
        "ms": "npm:ms@0.7.1"
      }
    },
    "npm:glob@7.0.5": {
      "map": {
        "inherits": "npm:inherits@2.0.1",
        "minimatch": "npm:minimatch@3.0.2",
        "path-is-absolute": "npm:path-is-absolute@1.0.0",
        "once": "npm:once@1.3.3",
        "inflight": "npm:inflight@1.0.5",
        "fs.realpath": "npm:fs.realpath@1.0.0"
      }
    },
    "npm:rimraf@2.5.2": {
      "map": {
        "glob": "npm:glob@7.0.5"
      }
    },
    "npm:strip-ansi@3.0.1": {
      "map": {
        "ansi-regex": "npm:ansi-regex@2.0.0"
      }
    },
    "npm:minimatch@3.0.2": {
      "map": {
        "brace-expansion": "npm:brace-expansion@1.1.5"
      }
    },
    "github:jspm/nodelibs-url@0.2.0-alpha": {
      "map": {
        "url-browserify": "npm:url@0.11.0"
      }
    },
    "npm:fsevents@1.0.12": {
      "map": {
        "nan": "npm:nan@2.3.5",
        "node-pre-gyp": "npm:node-pre-gyp@0.6.28"
      }
    },
    "npm:has-ansi@2.0.0": {
      "map": {
        "ansi-regex": "npm:ansi-regex@2.0.0"
      }
    },
    "npm:pinkie-promise@2.0.1": {
      "map": {
        "pinkie": "npm:pinkie@2.0.4"
      }
    },
    "npm:mkdirp@0.5.1": {
      "map": {
        "minimist": "npm:minimist@0.0.8"
      }
    },
    "github:jspm/nodelibs-http@0.2.0-alpha": {
      "map": {
        "http-browserify": "npm:stream-http@2.3.0"
      }
    },
    "npm:readable-stream@2.0.6": {
      "map": {
        "inherits": "npm:inherits@2.0.1",
        "isarray": "npm:isarray@1.0.0",
        "util-deprecate": "npm:util-deprecate@1.0.2",
        "process-nextick-args": "npm:process-nextick-args@1.0.7",
        "string_decoder": "npm:string_decoder@0.10.31",
        "core-util-is": "npm:core-util-is@1.0.2"
      }
    },
    "npm:readable-stream@2.1.4": {
      "map": {
        "inherits": "npm:inherits@2.0.1",
        "isarray": "npm:isarray@1.0.0",
        "buffer-shims": "npm:buffer-shims@1.0.0",
        "util-deprecate": "npm:util-deprecate@1.0.2",
        "process-nextick-args": "npm:process-nextick-args@1.0.7",
        "string_decoder": "npm:string_decoder@0.10.31",
        "core-util-is": "npm:core-util-is@1.0.2"
      }
    },
    "npm:inflight@1.0.5": {
      "map": {
        "once": "npm:once@1.3.3",
        "wrappy": "npm:wrappy@1.0.2"
      }
    },
    "npm:node-pre-gyp@0.6.28": {
      "map": {
        "mkdirp": "npm:mkdirp@0.5.1",
        "rimraf": "npm:rimraf@2.5.2",
        "request": "npm:request@2.72.0",
        "semver": "npm:semver@5.1.0",
        "nopt": "npm:nopt@3.0.6",
        "npmlog": "npm:npmlog@2.0.4",
        "tar-pack": "npm:tar-pack@3.1.3",
        "rc": "npm:rc@1.1.6",
        "tar": "npm:tar@2.2.1"
      }
    },
    "github:jspm/nodelibs-crypto@0.2.0-alpha": {
      "map": {
        "crypto-browserify": "npm:crypto-browserify@3.11.0"
      }
    },
    "npm:once@1.3.3": {
      "map": {
        "wrappy": "npm:wrappy@1.0.2"
      }
    },
    "github:jspm/nodelibs-zlib@0.2.0-alpha": {
      "map": {
        "zlib-browserify": "npm:browserify-zlib@0.1.4"
      }
    },
    "github:jspm/nodelibs-stream@0.2.0-alpha": {
      "map": {
        "stream-browserify": "npm:stream-browserify@2.0.1"
      }
    },
    "npm:brace-expansion@1.1.5": {
      "map": {
        "balanced-match": "npm:balanced-match@0.4.1",
        "concat-map": "npm:concat-map@0.0.1"
      }
    },
    "npm:url@0.11.0": {
      "map": {
        "punycode": "npm:punycode@1.3.2",
        "querystring": "npm:querystring@0.2.0"
      }
    },
    "github:jspm/nodelibs-buffer@0.2.0-alpha": {
      "map": {
        "buffer-browserify": "npm:buffer@4.6.0"
      }
    },
    "npm:stream-http@2.3.0": {
      "map": {
        "inherits": "npm:inherits@2.0.1",
        "readable-stream": "npm:readable-stream@2.1.4",
        "xtend": "npm:xtend@4.0.1",
        "to-arraybuffer": "npm:to-arraybuffer@1.0.1",
        "builtin-status-codes": "npm:builtin-status-codes@2.0.0"
      }
    },
    "npm:request@2.72.0": {
      "map": {
        "qs": "npm:qs@6.1.0",
        "mime-types": "npm:mime-types@2.1.11",
        "caseless": "npm:caseless@0.11.0",
        "forever-agent": "npm:forever-agent@0.6.1",
        "extend": "npm:extend@3.0.0",
        "tunnel-agent": "npm:tunnel-agent@0.4.3",
        "bl": "npm:bl@1.1.2",
        "tough-cookie": "npm:tough-cookie@2.2.2",
        "hawk": "npm:hawk@3.1.3",
        "json-stringify-safe": "npm:json-stringify-safe@5.0.1",
        "oauth-sign": "npm:oauth-sign@0.8.2",
        "isstream": "npm:isstream@0.1.2",
        "combined-stream": "npm:combined-stream@1.0.5",
        "aws-sign2": "npm:aws-sign2@0.6.0",
        "stringstream": "npm:stringstream@0.0.5",
        "is-typedarray": "npm:is-typedarray@1.0.0",
        "har-validator": "npm:har-validator@2.0.6",
        "aws4": "npm:aws4@1.4.1",
        "node-uuid": "npm:node-uuid@1.4.7",
        "form-data": "npm:form-data@1.0.0-rc4",
        "http-signature": "npm:http-signature@1.1.1"
      }
    },
    "npm:crypto-browserify@3.11.0": {
      "map": {
        "inherits": "npm:inherits@2.0.1",
        "browserify-sign": "npm:browserify-sign@4.0.0",
        "create-hash": "npm:create-hash@1.1.2",
        "create-hmac": "npm:create-hmac@1.1.4",
        "diffie-hellman": "npm:diffie-hellman@5.0.2",
        "randombytes": "npm:randombytes@2.0.3",
        "public-encrypt": "npm:public-encrypt@4.0.0",
        "browserify-cipher": "npm:browserify-cipher@1.0.0",
        "create-ecdh": "npm:create-ecdh@4.0.0",
        "pbkdf2": "npm:pbkdf2@3.0.4"
      }
    },
    "npm:stream-browserify@2.0.1": {
      "map": {
        "inherits": "npm:inherits@2.0.1",
        "readable-stream": "npm:readable-stream@2.1.4"
      }
    },
    "npm:stream-browserify@1.0.0": {
      "map": {
        "inherits": "npm:inherits@2.0.1",
        "readable-stream": "npm:readable-stream@1.1.14"
      }
    },
    "npm:tar-pack@3.1.3": {
      "map": {
        "debug": "npm:debug@2.2.0",
        "once": "npm:once@1.3.3",
        "readable-stream": "npm:readable-stream@2.0.6",
        "rimraf": "npm:rimraf@2.5.2",
        "tar": "npm:tar@2.2.1",
        "fstream-ignore": "npm:fstream-ignore@1.0.5",
        "uid-number": "npm:uid-number@0.0.6",
        "fstream": "npm:fstream@1.0.10"
      }
    },
    "npm:buffer@4.6.0": {
      "map": {
        "isarray": "npm:isarray@1.0.0",
        "ieee754": "npm:ieee754@1.1.6",
        "base64-js": "npm:base64-js@1.1.2"
      }
    },
    "github:jspm/nodelibs-punycode@0.2.0-alpha": {
      "map": {
        "punycode-browserify": "npm:punycode@1.4.1"
      }
    },
    "npm:rc@1.1.6": {
      "map": {
        "minimist": "npm:minimist@1.2.0",
        "ini": "npm:ini@1.3.4",
        "strip-json-comments": "npm:strip-json-comments@1.0.4",
        "deep-extend": "npm:deep-extend@0.4.1"
      }
    },
    "npm:browserify-zlib@0.1.4": {
      "map": {
        "readable-stream": "npm:readable-stream@2.1.4",
        "pako": "npm:pako@0.2.8"
      }
    },
    "npm:npmlog@2.0.4": {
      "map": {
        "ansi": "npm:ansi@0.3.1",
        "are-we-there-yet": "npm:are-we-there-yet@1.1.2",
        "gauge": "npm:gauge@1.2.7"
      }
    },
    "npm:nopt@3.0.6": {
      "map": {
        "abbrev": "npm:abbrev@1.0.9"
      }
    },
    "npm:bl@1.1.2": {
      "map": {
        "readable-stream": "npm:readable-stream@2.0.6"
      }
    },
    "github:jspm/nodelibs-string_decoder@0.2.0-alpha": {
      "map": {
        "string_decoder-browserify": "npm:string_decoder@0.10.31"
      }
    },
    "npm:readable-stream@1.1.14": {
      "map": {
        "core-util-is": "npm:core-util-is@1.0.2",
        "isarray": "npm:isarray@0.0.1",
        "string_decoder": "npm:string_decoder@0.10.31",
        "inherits": "npm:inherits@2.0.1",
        "stream-browserify": "npm:stream-browserify@1.0.0"
      }
    },
    "npm:tar@2.2.1": {
      "map": {
        "inherits": "npm:inherits@2.0.1",
        "fstream": "npm:fstream@1.0.10",
        "block-stream": "npm:block-stream@0.0.9"
      }
    },
    "npm:mime-types@2.1.11": {
      "map": {
        "mime-db": "npm:mime-db@1.23.0"
      }
    },
    "npm:har-validator@2.0.6": {
      "map": {
        "chalk": "npm:chalk@1.1.3",
        "pinkie-promise": "npm:pinkie-promise@2.0.1",
        "is-my-json-valid": "npm:is-my-json-valid@2.13.1",
        "commander": "npm:commander@2.9.0"
      }
    },
    "npm:browserify-sign@4.0.0": {
      "map": {
        "inherits": "npm:inherits@2.0.1",
        "create-hash": "npm:create-hash@1.1.2",
        "create-hmac": "npm:create-hmac@1.1.4",
        "bn.js": "npm:bn.js@4.11.4",
        "browserify-rsa": "npm:browserify-rsa@4.0.1",
        "parse-asn1": "npm:parse-asn1@5.0.0",
        "elliptic": "npm:elliptic@6.3.1"
      }
    },
    "npm:create-hash@1.1.2": {
      "map": {
        "inherits": "npm:inherits@2.0.1",
        "sha.js": "npm:sha.js@2.4.5",
        "cipher-base": "npm:cipher-base@1.0.2",
        "ripemd160": "npm:ripemd160@1.0.1"
      }
    },
    "npm:create-hmac@1.1.4": {
      "map": {
        "inherits": "npm:inherits@2.0.1",
        "create-hash": "npm:create-hash@1.1.2"
      }
    },
    "npm:diffie-hellman@5.0.2": {
      "map": {
        "randombytes": "npm:randombytes@2.0.3",
        "bn.js": "npm:bn.js@4.11.4",
        "miller-rabin": "npm:miller-rabin@4.0.0"
      }
    },
    "npm:public-encrypt@4.0.0": {
      "map": {
        "create-hash": "npm:create-hash@1.1.2",
        "randombytes": "npm:randombytes@2.0.3",
        "bn.js": "npm:bn.js@4.11.4",
        "browserify-rsa": "npm:browserify-rsa@4.0.1",
        "parse-asn1": "npm:parse-asn1@5.0.0"
      }
    },
    "npm:hawk@3.1.3": {
      "map": {
        "boom": "npm:boom@2.10.1",
        "sntp": "npm:sntp@1.0.9",
        "hoek": "npm:hoek@2.16.3",
        "cryptiles": "npm:cryptiles@2.0.5"
      }
    },
    "npm:combined-stream@1.0.5": {
      "map": {
        "delayed-stream": "npm:delayed-stream@1.0.0"
      }
    },
    "npm:fstream-ignore@1.0.5": {
      "map": {
        "inherits": "npm:inherits@2.0.1",
        "minimatch": "npm:minimatch@3.0.2",
        "fstream": "npm:fstream@1.0.10"
      }
    },
    "npm:fstream@1.0.10": {
      "map": {
        "graceful-fs": "npm:graceful-fs@4.1.4",
        "inherits": "npm:inherits@2.0.1",
        "mkdirp": "npm:mkdirp@0.5.1",
        "rimraf": "npm:rimraf@2.5.2"
      }
    },
    "npm:are-we-there-yet@1.1.2": {
      "map": {
        "readable-stream": "npm:readable-stream@1.1.14",
        "delegates": "npm:delegates@1.0.0"
      }
    },
    "npm:gauge@1.2.7": {
      "map": {
        "ansi": "npm:ansi@0.3.1",
        "lodash.pad": "npm:lodash.pad@4.4.0",
        "lodash.padend": "npm:lodash.padend@4.5.0",
        "has-unicode": "npm:has-unicode@2.0.0",
        "lodash.padstart": "npm:lodash.padstart@4.5.0"
      }
    },
    "npm:form-data@1.0.0-rc4": {
      "map": {
        "combined-stream": "npm:combined-stream@1.0.5",
        "mime-types": "npm:mime-types@2.1.11",
        "async": "npm:async@1.5.2"
      }
    },
    "npm:create-ecdh@4.0.0": {
      "map": {
        "bn.js": "npm:bn.js@4.11.4",
        "elliptic": "npm:elliptic@6.3.1"
      }
    },
    "npm:pbkdf2@3.0.4": {
      "map": {
        "create-hmac": "npm:create-hmac@1.1.4"
      }
    },
    "npm:browserify-cipher@1.0.0": {
      "map": {
        "browserify-des": "npm:browserify-des@1.0.0",
        "browserify-aes": "npm:browserify-aes@1.0.6",
        "evp_bytestokey": "npm:evp_bytestokey@1.0.0"
      }
    },
    "npm:http-signature@1.1.1": {
      "map": {
        "sshpk": "npm:sshpk@1.8.3",
        "assert-plus": "npm:assert-plus@0.2.0",
        "jsprim": "npm:jsprim@1.2.2"
      }
    },
    "npm:block-stream@0.0.9": {
      "map": {
        "inherits": "npm:inherits@2.0.1"
      }
    },
    "npm:boom@2.10.1": {
      "map": {
        "hoek": "npm:hoek@2.16.3"
      }
    },
    "npm:sntp@1.0.9": {
      "map": {
        "hoek": "npm:hoek@2.16.3"
      }
    },
    "npm:cryptiles@2.0.5": {
      "map": {
        "boom": "npm:boom@2.10.1"
      }
    },
    "npm:is-my-json-valid@2.13.1": {
      "map": {
        "xtend": "npm:xtend@4.0.1",
        "generate-function": "npm:generate-function@2.0.0",
        "generate-object-property": "npm:generate-object-property@1.2.0",
        "jsonpointer": "npm:jsonpointer@2.0.0"
      }
    },
    "npm:sha.js@2.4.5": {
      "map": {
        "inherits": "npm:inherits@2.0.1"
      }
    },
    "npm:browserify-des@1.0.0": {
      "map": {
        "inherits": "npm:inherits@2.0.1",
        "cipher-base": "npm:cipher-base@1.0.2",
        "des.js": "npm:des.js@1.0.0"
      }
    },
    "npm:browserify-aes@1.0.6": {
      "map": {
        "create-hash": "npm:create-hash@1.1.2",
        "inherits": "npm:inherits@2.0.1",
        "cipher-base": "npm:cipher-base@1.0.2",
        "evp_bytestokey": "npm:evp_bytestokey@1.0.0",
        "buffer-xor": "npm:buffer-xor@1.0.3"
      }
    },
    "github:jspm/nodelibs-domain@0.2.0-alpha": {
      "map": {
        "domain-browserify": "npm:domain-browser@1.1.7"
      }
    },
    "npm:sshpk@1.8.3": {
      "map": {
        "assert-plus": "npm:assert-plus@1.0.0",
        "asn1": "npm:asn1@0.2.3",
        "getpass": "npm:getpass@0.1.6",
        "dashdash": "npm:dashdash@1.14.0"
      }
    },
    "npm:browserify-rsa@4.0.1": {
      "map": {
        "bn.js": "npm:bn.js@4.11.4",
        "randombytes": "npm:randombytes@2.0.3"
      }
    },
    "npm:cipher-base@1.0.2": {
      "map": {
        "inherits": "npm:inherits@2.0.1"
      }
    },
    "npm:lodash.pad@4.4.0": {
      "map": {
        "lodash._baseslice": "npm:lodash._baseslice@4.0.0",
        "lodash._basetostring": "npm:lodash._basetostring@4.12.0",
        "lodash.tostring": "npm:lodash.tostring@4.1.3"
      }
    },
    "npm:lodash.padend@4.5.0": {
      "map": {
        "lodash._baseslice": "npm:lodash._baseslice@4.0.0",
        "lodash._basetostring": "npm:lodash._basetostring@4.12.0",
        "lodash.tostring": "npm:lodash.tostring@4.1.3"
      }
    },
    "npm:lodash.padstart@4.5.0": {
      "map": {
        "lodash._baseslice": "npm:lodash._baseslice@4.0.0",
        "lodash._basetostring": "npm:lodash._basetostring@4.12.0",
        "lodash.tostring": "npm:lodash.tostring@4.1.3"
      }
    },
    "npm:parse-asn1@5.0.0": {
      "map": {
        "browserify-aes": "npm:browserify-aes@1.0.6",
        "create-hash": "npm:create-hash@1.1.2",
        "pbkdf2": "npm:pbkdf2@3.0.4",
        "evp_bytestokey": "npm:evp_bytestokey@1.0.0",
        "asn1.js": "npm:asn1.js@4.6.2"
      }
    },
    "npm:miller-rabin@4.0.0": {
      "map": {
        "bn.js": "npm:bn.js@4.11.4",
        "brorand": "npm:brorand@1.0.5"
      }
    },
    "npm:evp_bytestokey@1.0.0": {
      "map": {
        "create-hash": "npm:create-hash@1.1.2"
      }
    },
    "npm:commander@2.9.0": {
      "map": {
        "graceful-readlink": "npm:graceful-readlink@1.0.1"
      }
    },
    "npm:elliptic@6.3.1": {
      "map": {
        "bn.js": "npm:bn.js@4.11.4",
        "inherits": "npm:inherits@2.0.1",
        "brorand": "npm:brorand@1.0.5",
        "hash.js": "npm:hash.js@1.0.3"
      }
    },
    "npm:des.js@1.0.0": {
      "map": {
        "inherits": "npm:inherits@2.0.1",
        "minimalistic-assert": "npm:minimalistic-assert@1.0.0"
      }
    },
    "npm:jsprim@1.2.2": {
      "map": {
        "extsprintf": "npm:extsprintf@1.0.2",
        "verror": "npm:verror@1.3.6",
        "json-schema": "npm:json-schema@0.2.2"
      }
    },
    "npm:getpass@0.1.6": {
      "map": {
        "assert-plus": "npm:assert-plus@1.0.0"
      }
    },
    "npm:jodid25519@1.0.2": {
      "map": {
        "jsbn": "npm:jsbn@0.1.0"
      }
    },
    "npm:ecc-jsbn@0.1.1": {
      "map": {
        "jsbn": "npm:jsbn@0.1.0"
      }
    },
    "npm:generate-object-property@1.2.0": {
      "map": {
        "is-property": "npm:is-property@1.0.2"
      }
    },
    "npm:asn1.js@4.6.2": {
      "map": {
        "bn.js": "npm:bn.js@4.11.4",
        "inherits": "npm:inherits@2.0.1",
        "minimalistic-assert": "npm:minimalistic-assert@1.0.0"
      }
    },
    "npm:verror@1.3.6": {
      "map": {
        "extsprintf": "npm:extsprintf@1.0.2"
      }
    },
    "npm:dashdash@1.14.0": {
      "map": {
        "assert-plus": "npm:assert-plus@1.0.0"
      }
    },
    "npm:hash.js@1.0.3": {
      "map": {
        "inherits": "npm:inherits@2.0.1"
      }
    }
  }
});
