# Tutorial: Tour of Heroes

This project is the Angular2 Tutorial, the webpack pipeline from the tutorial.

To test this project:

1. Run `npm i` to install dependencies
2. Run `npm start` to build the project, start a local server, and begin monitoring files for changes

[Tutorial Link](https://angular.io/docs/ts/latest/tutorial/)
