// mock data to test our app

import {Hero} from './hero';

export var HEROES: Hero[] = [
	{"id": 11, "name": "Botanica"},
	{"id": 12, "name": "Bombasto"},
	{"id": 13, "name": "Magentar"},
	{"id": 14, "name": "Deliriary" },
	{"id": 15, "name": "The Tornado"},
	{"id": 16, "name": "MineralAnimalVegetable Man"},
	{"id": 17, "name": "The Human Paper Cut"},
	{"id": 18, "name": "The Chasinator"},
	{"id": 19, "name": "The Program" },
	{"id": 20, "name": "Rex" }
]