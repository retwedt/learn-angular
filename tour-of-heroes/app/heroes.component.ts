// heroes.component


// import modules from angular core
import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router-deprecated';
// import custom modules
import {Hero} from './hero';
import {HeroService} from './hero.service';
import {HeroDetailComponent} from './hero-detail.component';


// setup angular component
@Component({
  selector: 'my-heroes',
  templateUrl: 'app/heroes.component.html',
	styleUrls: ['app/heroes.component.css'],
	directives: [HeroDetailComponent]
})


// export this HerosComponent class, this will hold
// variables and methods for your app
export class HeroesComponent implements OnInit {
	// declare variables
	heroes: Hero[];
	selectedHero: Hero;
	addingHero = false;
	error: any;

	// bind private instances to the constructor
	constructor(
		private heroService: HeroService,
		private router: Router
	) {}

	// Angular Lifecycle hooks
	ngOnInit() {
		this.getHeroes();
	}
	// Public Methods
	getHeroes() {
		// use the 'then' method of the Promise to execute a
		// callback when the promise has resolved
		this.heroService
				.getHeroes()
				.then(heroes => this.heroes = heroes)
				.catch(error => this.error = error); // TODO: display error message
	}
	// create a new hero
	addHero() {
		this.addingHero = true;
		this.selectedHero = null;
	}
	close(savedHero: Hero) {
		this.addingHero = false;
		if (savedHero) {
			this.getHeroes();
		}
	}
	// delete an existing hero
	delete(hero: Hero, event: any) {
		event.stopPropagation();
		this.heroService
				.delete(hero)
				.then(res => {
					this.heroes = this.heroes.filter(h => h !== hero);
					if (this.selectedHero === hero) {
						this.selectedHero = null;
					}
				})
				.catch(error => this.error = error); // TODO: display error message
	}
	onSelect(hero: Hero) {
		this.selectedHero = hero;
		this.addingHero = false;
	}
	gotoDetail() {
		let link = ['HeroDetail', { id: this.selectedHero.id }];
		this.router.navigate(link);
	}
}
